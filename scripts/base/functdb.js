/* BMPROJECT FILE: base/functdb.js - Function Test Database

SYNOPSIS:
	The Function Test Database keeps tabs of functions that need to be
	tested. Functions that need this testing have a special tag directly
	before their declaration. For an example, see the tag on the line
	before the TalkBox() function.

EXECUTION:
	Technically, there is no execution inherent in this file. Any function
	that needs to be added to the FTDB has a tag before it that adds it to
	the database. Without this file, however, that execution would not take
	place.

CONTENT:
	Contains the FTDB itself, along with declarations for the Test Record
	Object and the FTDB functions. These functions and objects should not
	be called outside of the function tags.

NOTES:
	Any internal functions that need to be tested should have their tags at
	the bottom of this file, with the special tag in the GetKeyString()
	example.

LICENSE(S):
	GNU General Public License (see zeroinit.js)

*/
// The Function Test Records, BM Project by dremelofdeath
// This database was implemented to enable testing of functions from
// the devmenu. The function tells how the devmenu should allow a tester
// to test the function.

var FTDB /*function test database*/ = new Array();

function TestRecordObject(functionname, argarray, linenumber, filename, needsflipped) {
	this.name = functionname;
	this.args = argarray;
	this.needsflipped = needsflipped;
	this.linenumber = linenumber;
	this.filename = filename;
}

function TestRecord(functionname, argarray, linenumber, filename, needsflipped) {
	if(needsflipped == undefined) needsflipped = false;
	FTDB.push(new TestRecordObject(functionname, argarray, linenumber, filename, needsflipped));
}

function EmulateFunctionExecution(functionname, argumentsarray) {
	// Make sure the argumentsarray contains the actual arguments to pass to the function, not the argument names
	var argstr = "";
	var evalret;
	// first, build the string of arguments for the eval script
	for(var i = 0; i < argumentsarray.length; i++) {
		argstr += argumentsarray[i];
		argstr = (i==argumentsarray.length-1)? argstr:argstr+",";
	}
	ClearScreenBuffer();
	eval("evalret = " + functionname + "(" + argstr + ");");
	return evalret;
}


TestRecord("GetKeyString",new Array("key","shift"),0,"(internal)",false);
try {notafunction();} catch(k) {TotalLineCount += k.lineNumber+1; FileLineCount[k.fileName.replace(/scripts\//i,"")] = k.lineNumber+1;}