/* BMPROJECT FILE: base/general.js - General Program Functions

SYNOPSIS:
	This file is a core component of many other BM scripts. Some of the
	functions included in this file are so generic they are used in almost
	every other script.

EXECUTION:
	None. This file only declares scripts used by other scripts and
	functions.

CONTENT:
	Contains only extremely generic or universally useful functions that
	apply (or could apply) to every other aspect of BM.

LICENSE(S):
	GNU General Public License (see zeroinit.js)

*/

// Betrayer's Moon dremelofdeath2K5GPL

function wait(ms) {
	var ahora = GetTime();
	while(GetTime() < ahora+ms) {}
}
function wait(ms,skip) {
	var ahora = GetTime();
	while(GetTime() < ahora+ms) {
		if(skip && AreKeysLeft()) if(GetKey() == KEY_ENTER) {
			return true;
		}
	}
	return false;
}
function waitc(ms,skip,ss) { // continues to FlipScreen() during the wait
	var ahora = GetTime();
	if(!ss || ss == undefined) var ss = GetScreenBuffer();
	while(GetTime() < ahora+ms) {
		if(skip && AreKeysLeft()) if(GetKey() == KEY_ENTER) {return true;}
		ss.blit(0,0);
		FlipScreen();
	}
	return false;
}
function waitup(ms,skip) { // updates the map engine during the wait
	var ahora = GetTime();
	while(GetTime() < ahora+ms) {
		if(skip && AreKeysLeft()) if(GetKey() == KEY_ENTER) {return true;}
		UpdateMapEngine();
		RenderMap();
		FlipScreen();
	}
	return false;
}
function waitcup(ms,ss,x,y,skip) { // a combination of waitc and waitup. renders the map,
  var ahora = GetTime();           // and then renders the image on top of the new map.
	while(GetTime() < ahora+ms) {
		if(skip && AreKeysLeft()) if(GetKey() == KEY_ENTER) {return true;}
		RenderMap();
		ss.blit(x,y);
		FlipScreen();
		UpdateMapEngine();
	}
	return false;
}
function waitfcup(ms,renderlo,skip) { // same as waitcup, but instead of an image it has a
  var ahora = GetTime();                    // prerendering function that is executed after the map
	while(GetTime() < ahora+ms) {             // is updated.
		if(skip && AreKeysLeft()) if(GetKey() == KEY_ENTER) {return true;}
		UpdateMapEngine();
		RenderMap();
		renderlo();
		FlipScreen();
	}
	return false;
}
function WaitForKeyPressC(key) {
	if(key == undefined) key = KEY_ENTER;
	while(!(AreKeysLeft() && IsKeyPressed(key))) {
		UpdateMapEngine();
		RenderMap();
		FlipScreen();
	}
	ClearKeyQueue();
}
function WaitForKeyPressFCUP(key,renderlo) {
	if(key == undefined) key = KEY_ENTER;
	while(!(AreKeysLeft() && IsKeyPressed(key))) {
		UpdateMapEngine();
		RenderMap();
		renderlo();
		FlipScreen();
	}
	ClearKeyQueue();
}

function TilesToPixels(number) {
	return GetTileHeight()*number+Math.round(GetTileHeight()/2);
}
function PixelsToTiles(number) {
	return Math.floor(number/GetTileHeight());
}
function UpdateAndRenderMap() {
	if(IsMapEngineRunning()) {
		UpdateMapEngine();
		RenderMap();
	}
}
function MoveRandomlyEX() {
 var commands = new Array(COMMAND_MOVE_NORTH, COMMAND_MOVE_SOUTH, COMMAND_MOVE_EAST, COMMAND_MOVE_WEST, COMMAND_WAIT, COMMAND_WAIT, COMMAND_WAIT, COMMAND_WAIT, COMMAND_WAIT, COMMAND_WAIT, COMMAND_WAIT);
 var r = Math.floor(Math.random() * commands.length);
 Move(GetCurrentPerson(), commands[r], 1, false);
}
function KeepBlittingUntilKeyHit(ss) {
	while(!(AreKeysLeft() && IsAnyKeyPressed())) {
		ss.blit(0,0);
	}
}

/* The GetArrayElementNames() function
GetArrayElementNames() returns an array of all of the children of its only parameter.

For example, say we have an array like so:

var Example = new Array();
Example["one"] = "asdf";
Example["two"] = "beef-stew";
Example["three"] = "meowmix";

GetArrayElementNames(Example); // returns {"one","two","three"}
*/

function GetArrayElementNames(own) {
	var childrenr = new Array();
	for(var sz in own) {
		childrenr.push(sz);
	}
	return childrenr;
}

/* The roundto() function
roundto() simply returns the number n, rounded to dpx decimal places.

roundto(Math.PI,2); // returns 3.14
*/
function roundto(n,dpx) {
	var numero = Math.pow(10,dpx); // BUGFIX: spherical ID# 1125
	return Math.round(n*numero)/numero;
}

function DegreesToRadians(degrees) {
  return (degrees * Math.PI / 180.0);
}
function RadiansToDegrees(radians) {
  return (radians * 180.0 / Math.PI);
}
function deg2rad(degrees) {
  return (degrees * Math.PI / 180.0);
}
function rad2deg(radians) {
  return (radians * 180.0 / Math.PI);
}
try {notafunction();} catch(k) {TotalLineCount += k.lineNumber+1; FileLineCount[k.fileName.replace(/scripts\//i,"")] = k.lineNumber+1;}