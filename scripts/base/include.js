/* BMPROJECT FILE: base/include.js - Script Inclusion Systems

SYNOPSIS:
	This script deprecates the use of EvaluateScript() and RequireScript()
	by using a systematic preloader to evaluate scripts.

EXECUTION:
	None until the preloader is called. It then reads the databases and
	loads all of the files loaded into the databases through the preloader.

CONTENT:
	Routines for adding scripts and support files to the include databases.
	Also contains several global variables that pertain to the include
	process.

NOTES:
	The includes are declared in exectrl.js.

LICENSE(S):
	GNU General Public License (see zeroinit.js)

*/
// Betrayer's Moon Project Includes Library
// Started 4:03 PM EST 10/30/06, by Zachary Murray/dremelofdeath

var Fonts = new Array();
var WindowStyles = new Array();
var Includes = new Array();
var Priorities = new Array();
var SoundFX = new Array();

const InternalTextFont = "FF6font";
const InternalCaptFont = "Square1";
const InternalTitleFont = "dscar48";
var TextFont,CaptFont,TitleFont;

var DefaultStartStyle = "Chara";

// The Include Library needs to make sure to not reference any
// function that has not yet been loaded.

function prewait(ms) {
	var clock = GetTime();
	while(GetTime() < clock+ms) {}
}

function IncludeScript(script) {
	Includes.push(script);
}

function IncludeFont(font) {
	Fonts.push(font);
}

//IncludeFont("System.rfn");
IncludeFont("FF6font.rfn");
IncludeFont("Square1.rfn");
IncludeFont("dscar48.rfn"); // Used with permission. Copyright Dennis Ludlow, all rights reserved.

if(ShowPreloader) {
	GetSystemFont().drawText(0,10,"Now loading Fonts, please wait.");
		if(StepThroughLoad) {
			GetSystemFont().drawText(0,GetScreenHeight()-(GetSystemFont().getHeight()*2)-10,"NOTICE: Build option StepThroughLoad is active");
			GetSystemFont().drawText(0,GetScreenHeight()-GetSystemFont().getHeight()-10,"Press any key to continue load process");
	}
	FlipScreen();
}
if(StepThroughLoad) GetKey();
for(var i = 0; i < Fonts.length; i++) {
	eval("const " + Fonts[i].match(/(\w+)\w/i)[0] + " = LoadFont(\"" + Fonts[i] + "\");");
}
eval("TextFont = " + InternalTextFont + ";");
eval("CaptFont = " + InternalCaptFont + ";");
eval("TitleFont = " + InternalTitleFont + ";");

function IncludeStyle(style) {
	WindowStyles.push(style);
}

IncludeStyle("Chara.rws");
IncludeStyle("Chara0.rws");
IncludeStyle("Chara1.rws");
IncludeStyle("Chara2.rws");
IncludeStyle("Chara3.rws");

function SetDefaultStartStyle(style) {
	DefaultStartStyle = style;
}

var StartWindowStyle = DefaultStartStyle;

// Perhaps load the user's preference of startup window style color??
// Zach: Like... this?
  if(ShowPreloader) {
	TextFont.drawText(0,10,"Now loading WindowStyles, please wait.");
	if(StepThroughLoad) {
		TextFont.drawText(0,GetScreenHeight()-(TextFont.getHeight()*2)-10,"NOTICE: Build option StepThroughLoad is active");
		TextFont.drawText(0,GetScreenHeight()-TextFont.getHeight()-10,"Press any key to continue load process");
	}
	FlipScreen();
}
if(StepThroughLoad) GetKey();
for(var i = 0; i < WindowStyles.length; i++) {
	eval("const " + WindowStyles[i].match(/(\w+)\w/i)[0] + " = LoadWindowStyle(\"" + WindowStyles[i] + "\");");
}
var GetStartUpWindowStyle = PreferencesFile.read("StartUpWindowStyle",DefaultStartStyle); // default loading screen window style preference string
eval("StartWindowStyle = " + GetStartUpWindowStyle + ";"); // evaluate the file contents

function IncludePriority(script) {
	Priorities.push(script);
}

function IncludeSystem(systemscript) {
	EvaluateSystemScript(systemscript);
}

var Licenses = new Array();

function IncludeScriptOtherLicense(script,copyright_notice) {
	Licenses.push(script + ": " + copyright_notice);
	IncludeScript(script);
}

function CallScriptLoader() {
	// Me: *ghostly voice* I HAVE A SUNDIAL PROCESSOR! IT'S FOUR IN THE
	//     MORNING!!
	// Priority scripts are evaluated here.
	for(var s = 0; s < Priorities.length; s++) {
		EvaluateScript(Priorities[s]);
	}
	/*if(ShowPreloader) {
		FlashBox(StartWindowStyle,"Ready.","NOW LOADING","middle");
		FlipScreen();
	}*/
	if(ForcePreloaderSlow) prewait(PreloaderSlowdown);
	// Now, one by one, evaluate every one of the includes
	var jp = 0; // Thank you, Kevin. jp just fixed a huge infinite loop!
	do {
		if(ShowPreloader) {
			FlashBox(StartWindowStyle,"Loading...\nStarting program... " + Math.ceil(((jp+1)/Includes.length)*100) + "%\nComponent " + Includes[jp] + "...","NOW LOADING","middle");
			FlipScreen();
		}
		EvaluateScript(Includes[jp]);
		if(ForcePreloaderSlow) prewait(PreloaderSlowdown);
		jp++;
	} while(jp < Includes.length);
}

function IncludeSFX(sfx) {
	SoundFX.push(sfx);
} // Sound effects are loaded in soundfx.js
try {notafunction();} catch(k) {TotalLineCount += k.lineNumber+1; FileLineCount[k.fileName.replace(/scripts\//i,"")] = k.lineNumber+1;}