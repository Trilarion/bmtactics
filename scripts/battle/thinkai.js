/* BMPROJECT FILE: battle/thinkai.js - Artificial Intelligence Thinking Routines
SYNOPSIS:
	This script controls how the AI system functions. It defines the system
	itself.

EXECUTION:
	None - only defines the AI system.

CONTENT:
	Contains the AI framework for how the system "thinks": deciding out of
	possible decisions which one to take, based on probability and other
	various factors.

SEE ALSO:
	battle/synapse.js - Specific thought styles

LICENSE(S):
	GNU General Public License (see zeroinit.js)

*/

try {_notafunction();} catch(error) {postnote("Please keep in mind that the AI system is not complete and is not functional.",error);}