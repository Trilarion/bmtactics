/* BMPROJECT FILE: ioctrl/errorh.js - Error Handling Support

SYNOPSIS:
	This script handles and parses errors that are thrown by other scripts.

EXECUTION:
	None - HandleException() is called only when an error is encountered.

CONTENT:
	Contains the error handler scripts (and also a hack to fix Exit() calls
	during header processing).

NOTES:
	The error handler should be called in the catch block of try/catch.
	Also, you need two separate try/catch blocks: one for the headers and
	one for game() internals.

LICENSE(S):
	GNU General Public License (see zeroinit.js)

*/
// Betrayer's Moon Project Error Handler, by dremelofdeath.
// This will simply interpret the errors thrown by Javascript.
// This will replace the engine's internal error handler.

var OMG_ERROR = false; // part of a very ugly hack to fix Exit() before game()

function HandleException(exception) { // this thing is freakin' bulletproof now!
	var fonttouse = TextFont? TextFont : GetSystemFont();
	var titlefontuse = TitleFont? TitleFont : GetSystemFont();
	var texttodisplay = GameTitle;
	var btextx,btexty;
	if(typeof FlushScreenBuffer == "function") { // backporting some improvements from fixme
		FlushScreenBuffer();
	} else {
		FlipScreen();
		FlipScreen();
	}
	if(typeof ErrorBox == "function") { // nice, eh? load autodetection
		fonttouse.drawTextBox(0,GetSystemFont().getHeight(),GetScreenWidth(),GetScreenHeight(),0,exception.stack);
		btextx = GetScreenWidth() - titlefontuse.getStringWidth(texttodisplay);
		btexty = GetScreenHeight() - titlefontuse.getHeight();
		titlefontuse.drawText(btextx,btexty,texttodisplay);
		var ss = GetScreenBuffer();
		CreateSurface(GetScreenWidth(),GetScreenHeight(),CreateColor(0,0,0)).blit(0,0);
		ss.blitMask(0,0,CreateColor(100,100,100));
		ErrorBox("The program has encountered a critical error (" + exception.name + "). Any unsaved data in the program has been lost.\n\n" + exception.message,"ERROR: " + exception.fileName.toUpperCase() + ", LINE " + (exception.lineNumber+1));
	} else {
		fonttouse.drawTextBox(0,20,GetScreenWidth(),GetScreenHeight()-20,0,"ERROR: " + exception.fileName.toUpperCase() + ", LINE " + (exception.lineNumber+1)+"\n\n"+"The program has encountered a critical error (" + exception.name + "). Any unsaved data in the program has been lost.\n\n" + exception.message);
	}
	FlipScreen();
	while(AreKeysLeft()) {void GetKey();}
	GetKey();
	//Exit(); // See set_die_flag();
}

function HandleTestException(exception) { // modified version of HandleException()
	ClearScreenBuffer();
	ErrorBox("It appears that the program encountered an error while testing a function (" + exception.name + "). The program intentionally did not shut down. If you feel the error could damage data, manually exit.\n\n" + exception.message,"ERROR: " + exception.fileName.toUpperCase() + ", LINE " + (exception.lineNumber+1));
	return true;
}

// !!!!!!! UGLY HACK WARNING !!!!!!! //
function set_die_flag() {OMG_ERROR = true;}
function check_die_flag() {if(OMG_ERROR) Exit();}
try {notafunction();} catch(k) {TotalLineCount += k.lineNumber+1; FileLineCount[k.fileName.replace(/scripts\//i,"")] = k.lineNumber+1;}