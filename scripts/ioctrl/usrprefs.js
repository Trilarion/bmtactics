/* BMPROJECT FILE: usrprefs.js - User Controlled Preferences and Options

SYNOPSIS:
	This file maintains the preferences and options available to the user.

EXECUTION: finish
	This script defines all of the user accessible options, such as
	difficulty, windowstyle, and the like.
	before game(), thus cleaning up all of the scattered information there.

CONTENT:
	Contains the UserPreference object and all of the user preferences in
	the game.

NOTES:
	Please keep in mind that you cannot change the current value when
	declaring the preference; it is automatically set to the default value.
	This also means that the settings that are declared in the definition
	are the defaults for new games and new saves. 

LICENSE(S):
	GNU General Public License (see zeroinit.js)

*/

var userprefs = new Array();

function UserPreference(optdefault,arrayofpossibleopts) {
	this.current = optdefault;
	this.content = this.current;
	this.optdefault = optdefault;
	this.arrayofpossibleopts = arrayofpossibleopts;
	this.options = this.arrayofpossibleopts;
}

UserPreference.prototype.set = function(optiontoset) {
	var check = false;
	if((this.options != undefined) && (typeof this.options.splice == "function")) {
		for(var i = 0; i < this.options; i++) {
			if(optiontoset == this.options[i]) {
				check = true;
				break;
			}
		}
	}
	if(check) this.current = optiontoset;
}

UserPreference.prototype.reset = function() {
	this.current = this.optdefault;
}


////////// USER ACCESSIBLE AREA //////////

userprefs["style"] = new UserPreference(Chara,new Array(Chara,Chara0,Chara1,Chara2,Chara3));
userprefs["upkey"] = new UserPreference(KEY_UP,AllKeys);
userprefs["downkey"] = new UserPreference(KEY_DOWN,AllKeys);
userprefs["leftkey"] = new UserPreference(KEY_LEFT,AllKeys);
userprefs["rightkey"] = new UserPreference(KEY_RIGHT,AllKeys);
userprefs["actionkey"] = new UserPreference(KEY_ENTER,AllKeys);
userprefs["cancelkey"] = new UserPreference(KEY_ESCAPE,AllKeys);

////////// END USER ACCESSIBLE AREA //////////
try {notafunction();} catch(k) {TotalLineCount += k.lineNumber+1; FileLineCount[k.fileName.replace(/scripts\//i,"")] = k.lineNumber+1;}