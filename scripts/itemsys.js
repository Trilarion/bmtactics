// itemsys.js dremelofdeath

const ITEM_TYPE_CONSUMABLE 	= 2;
const ITEM_TYPE_WEAPON			= 3;
const ITEM_TYPE_BODY				= 5;
const ITEM_TYPE_HAND				= 7;
const ITEM_TYPE_HEAD				= 11;
const ITEM_TYPE_SHOES				= 13;

function Item