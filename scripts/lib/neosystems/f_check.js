// boolean check functions
/********************************************************************************
 IsWhitespace() - return true if a char is a line break, space, or bell(?)
 isAlpha() - return true if a char is alphabetic
 IsChar() - return true if an obj is a char or string
 IsString() - return true if an obj is a char or string
 IsInt() - return true if an obj is an int
 IsArray() - return true if an obj is an array
 IsSound() - return true if obj is a soundobject
 IsFunction() - return true if an obj is a function
 function_exists() - return true if function f is defined
 *******************************************************************************/
function IsWhitespace(ch) {
 var ret = false;
 switch (ch) {
  case '\n': case '\r': case ' ': case '\b': ret = true;
  }
 return ret;
 }

function isAlpha(ch) {
 var upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
 var lower = "abcdefghijklmnopqrstuvwxyz";
 for (var i=0; i<upper.length; i++)
  if (ch==upper.charAt(i)||ch==lower.charAt(i)) return true;
 return false;
 }

function IsChar(ch) {
 // for now will only handle ASCII (32-255)
 // returns false if ch is NOT a single char string
 if (ch.length==1) {
  return (typeof ch=="string"||typeof ch=="char"||IsWhitespace(ch));
  }
 return false;
 }

function IsString(str) {
 return (typeof str=="string"||typeof str=="char");
 }

function IsInt(num) {
 return (num!=undefined&&num!=NaN&&num==parseInt(num));
 }

function IsArray(arr) {
 return (typeof(arr)=="array");
 }

function IsSound(snd) {
 return (typeof(snd)==typeof(LoadSound("00_drop.wav",false)));
 }
function IsNumber(n) {
 return (typeof(n)=="number"||typeof(n)=="integer");
 }

function IsImage(n) {
 return (typeof(n)==typeof(GetSystemArrow()));
 }

function IsFont(n) {
 return (typeof(n)==typeof(GetSystemFont()));
 }

function IsColor(n) {
 return (typeof(n)==typeof(CreateColor(0,0,0)));
 }

function IsWindowStyle(n) {
 return (typeof(n)==typeof(GetSystemWindowStyle()));
 }

function IsSpriteset(n) {
 return (typeof(n)==typeof(LoadSpriteset("c_pengu.rss")));
 }

function IsFunction(func) {
 return (typeof(func)==typeof(function(){}));
 }

function IsDefined(v) {
 return (v!=undefined&&v!=null);
 }

function function_exists(func) {
 // must put the name of the function WITHOUT any "s or 's
 // ie function_exists(Abort)
 return (IsDefined(func)&&IsFunction(func));
 }

function bool2str(b) {
 if (b) return 1;
 else return 0;
 }

function kill(what,min,max,name) {
 if (!name) name = "some var passed";
 if (what<min||what>max)
  Abort(name+" "+what+" not between "+min+" & "+max);
 }

function Assert(line) {
 if (eval(line)==false)
  Abort("Assert: "+line+" failed");
 }

