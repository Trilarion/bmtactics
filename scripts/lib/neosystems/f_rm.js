// RPGMAKER-STYLE FUNCTIONS

// EVENT FUNCTIONS
function choice(alignH,alignV,mSrc,prompt,arr,mSrcEsc) {
 tmpMenu.reset();
 tmpMenu.escape_function = function() {
  //RenderMap();
  //Delay(200);
  this.escaped = true;
  this.done = true;
  if (!mSrcEsc) mSrc.execute();
  else mSrc.escape_function();
  }
 tmpMenu.background = ScrShot();
 tmpMenu.items = [];
 for (var prop in arr) {
  tmpMenu.addText(prop,new Function(arr[prop]));
  }
 tmpMenu.constW = tmpMenu.getWidth();
 tmpMenu.constH = tmpMenu.getHeight();
 var tmpPrompt = '';
 tmpPrompt = prompt.toString();
 tmpMenu.preRender = new Function("tmpMenu.newPreRender('CHOICE','"+tmpPrompt+"');");
 tmpMenu.align(alignH,alignV);
 return tmpMenu.execute();
 }

function Warp(x, y, layer, map) { // basic teleport
 if (IsInputAttached()) {var person = GetInputPerson();}
 else {return false;}
 if (map) ChangeMap(map);
 if (layer >= 0) SetPersonLayer(person, layer);
 SetPersonX(person, x);
 SetPersonY(person, y);
 }

function WarpParty(x,y,layer,map) { 
 if (db.party.length<1) return false;
 if (map) ChangeMap(map);
 for (var i=0; i<db.party.length; i++) {
  var person = db.party[i].id;
  if (layer>=0) SetPersonLayer(person, layer);
  SetPersonX(person,x);
  SetPersonY(person,y);
  }
 }

function WarpObject(x, y, layer, map) { // an obj that warps
 if (this instanceof WarpObject == false) {
  return new WarpObject(x, y, layer, map);
  }
 this.x = x;
 this.y = y;
 this.layer = layer;
 this.map = map;
 }

WarpObject.prototype.warp_to = function() { // teleport the obj
 var person = GetInputPerson();
 if (this.map) ChangeMap(this.map);
 if (this.layer >= 0) SetPersonLayer(person, this.layer);
 SetPersonX(person, this.x);
 SetPersonY(person, this.y);
 }

function FadeWarp(x, y, layer, map) { // fade warp
 //require("screen.js") for fade
 FadeOut(1000);
 Warp(x, y, layer, map);
 UpdateMapEngine();
 RenderMap();
 FadeIn(1000);
 }

function songFadeOut(mus,endVol,rate) { // fade out a song
 // the higher the rate the smoother the fadeout
 // if rate is 0 cut song immediately
 if (rate>0) {
  var step = (mus.getVolume()-endVol)/rate;
  while (mus.getVolume()>endVol) {
   mus.setVolume(mus.getVolume()-step);
   Delay(100);
   }
  }
 mus.setVolume(endVol);
 mus.stop();
 }

function imageFadeOut(pic,x,y,startOp,ms) {
 var a = 0;
 var img = ScrShot();
 if (ms>0) {
  var cStep = (startOp-a)/ms;
  var time = GetTime();
  var time2 = GetTime()
  while (time2-time<ms) {
   a = (time2-time)*cStep;
   img.blit(0,0);
   pic.blitMask(x,y,CreateColor(255,255,255,startOp-a));
   FlipScreen();
   time2 = GetTime();
   }
  }
 img.blit(0,0);
 pic.blitMask(x,y,CreateColor(255,255,255,0));
 FlipScreen();
 img.blit(0,0);
 pic.blitMask(x,y,CreateColor(255,255,255,0));
 }


function songFadeIn(mus,endVol,rate,loop) { // fade in a song
 // the higher the rate the smoother the fadein
 // if rate is 0 start song immediately
 mus.setVolume(0);
 mus.play(loop);
 if (rate>0) {
  var step = endVol/rate;
  while (mus.getVolume()<endVol) {
   mus.setVolume(mus.getVolume()+step);
   Delay(100);
   }
  }
 mus.setVolume(endVol);
 }

function imageFadeIn(pic,x,y,endOp,ms) {
 var a = 0;
 var img = ScrShot();
 if (ms>0) {
  var cStep = (endOp-a)/ms;
  var time = GetTime();
  var time2 = GetTime()
  while (time2-time<ms) {
   a = (time2-time)*cStep;
   img.blit(0,0);
   pic.blitMask(x,y,CreateColor(255,255,255,a));
   FlipScreen();
   time2 = GetTime();
   }
  }
 img.blit(0,0);
 pic.blitMask(x,y,CreateColor(255,255,255,endOp));
 FlipScreen();
 img.blit(0,0);
 pic.blit(x,y);
 }


function MoveInPlaceUpdate(name,dir) {
 var ss = GetPersonSpriteset(name);
 var lastF = GetPersonFrame(name);
 var lastD = GetPersonDirection(name);
 if (dir!=lastD) SetPersonDirection(name,dir);
 var last_update = GetTime();
 if ("firstcall" in MoveInPlaceUpdate == false) { 
  MoveInPlaceUpdate.last_update = new Object(); 
  MoveInPlaceUpdate.firstcall = true; 
  } 
 if (name in MoveInPlaceUpdate.last_update)
  last_update = MoveInPlaceUpdate.last_update[name]; 
 else MoveInPlaceUpdate.last_update[name] = last_update; 
 if (dir==lastD) var i = lastF+1;
 else i = 0;
 var d = findDirIndex(ss,dir);
 i = i%ss.directions[d].frames.length;
 var delay = ss.directions[d].frames[i].delay;
 var dRatio = 10;
 var changed = false;
 if (last_update+delay*dRatio<GetTime()) {
  SetPersonFrame(name,i);
  changed = true;
  }
 if (changed) MoveInPlaceUpdate.last_update[name] = GetTime();
 }

function findDirIndex(ss,dir) {
 for (var i=0; i<ss.directions.length; i++) {
  if (ss.directions[i].name==dir) return i;
  }
 return 0;
 }

// EVENT CONDITION CHECKERS
function IsInParty(id) {
 for (var i=0; i<db.party.length; i++)
  if (db.party[i].id==id) return true;
 return false;
 }

// MODIFIERS
function ReturnFace(img,whichH,whichV,w,h) {
 var pic;
 if (IsImage(img)) pic = img;
 else pic = LoadImage(img.toString());
 w = (w)?w:FW; h = (h)?h:FH;
 var theX = whichH*w;
 var theY = whichV*h;
 var s = pic.createSurface();
 var ss = s.cloneSection(theX,theY,w,h);
 return ss.createImage();
 }

// FLIK MOVE IN PLACE ANIMATIONS
function __GetPersonSpriteset__(name) { 
 if ("firstcall" in __GetPersonSpriteset__ == false) { 
  __GetPersonSpriteset__.spritesets = new Object(); 
  __GetPersonSpriteset__.firstcall = true; 
  } 
 if (name in __GetPersonSpriteset__.spritesets) { 
  return __GetPersonSpriteset__.spritesets[name]; 
  } 
 else { 
  __GetPersonSpriteset__.spritesets[name] = GetPersonSpriteset(name); 
  return __GetPersonSpriteset__.spritesets[name]; 
  } 
 return null; 
 } 

/////////////////////////////////////////////////////////// 

function UpdatePersonAnimation(name) { 
 var spriteset = __GetPersonSpriteset__(name); 
 var direction_name = GetPersonDirection(name); 
 var direction_index = -1; 
 var frame_index = GetPersonFrame(name); 
 var changed = false; 
 var last_update = GetTime(); 
 if ("firstcall" in UpdatePersonAnimation == false) { 
  UpdatePersonAnimation.last_update = new Object(); 
  UpdatePersonAnimation.firstcall = true; 
  } 
 if (name in UpdatePersonAnimation.last_update) { 
  last_update = UpdatePersonAnimation.last_update[name]; 
  } 
 else { 
  UpdatePersonAnimation.last_update[name] = last_update; 
  } 
 for (var i = 0; i < spriteset.directions.length; i++) { 
  if (spriteset.directions[i].name == direction_name) { 
   direction_index = i; 
   break; 
   } 
  } 
 if (direction_index > -1) { 
  if (frame_index >= spriteset.directions[direction_index].frames.length) 
   frame_index = 0; 
  if (last_update + (spriteset.directions[direction_index].frames[frame_index].delay * 10) <       GetTime()) { 
   changed = true; 
   frame_index += 1; 
   if (frame_index >= spriteset.directions[direction_index].frames.length) 
    frame_index = 0; 
   SetPersonFrame(name, frame_index); 
   } 
  } 
 if (changed) { 
  UpdatePersonAnimation.last_update[name] = GetTime(); 
  } 
 return changed;   
 } 
