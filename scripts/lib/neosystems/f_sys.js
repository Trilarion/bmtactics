function rotate(prop,arr) {
 for (var i=0; i<arr.length; i++) {
  if (prop==arr[i])  {if (i==arr.length-1) i=-1; prop = arr[i+1]; break;}
  }
 return prop;
 }

function ClearKeyQueue() {
 while(AreKeysLeft()) GetKey();
 }

function BiosKeyPressed(key) {
 return (AreKeysLeft() && IsKeyPressed(key));
 }

function GetSpritesetHeight(ss) {
 var s, h = 0;
 if (IsString(ss)) s = LoadSpriteset(ss);
 else s = ss;
 for (var i=0; i<s.images.length; i++) {
  if (s.images[i].height>h) h = s.images[i].height;
  }
 return h;
 }
function GetSpritesetWidth(ss) {
 var s, w = 0;
 if (IsString(ss)) s = LoadSpriteset(ss);
 else s = ss;
 for (var i=0; i<s.images.length; i++) {
  if (s.images[i].width>w) w = s.images[i].width;
  }
 return w;
 }
function GetBaseHeight(ss) {
 var s;
 if (IsString(ss)) s = LoadSpriteset(ss);
 else s = ss;
 return Math.abs(s.base.x2-s.base.x1)+1;
 }
function GetBaseWidth(ss) {
 var s;
 if (IsString(ss)) s = LoadSpriteset(ss);
 else s = ss;
 return Math.abs(s.base.y2-s.base.y1)+1;
 }

// image functions
function ResetMask() {
 var color = CreateColor(0,0,0,0);
 ApplyColorMask(color);
 }

function blit(img,x,y,delay) { // basic blit
 var pic = (IsString(img))?LoadImage(img):img;
 if (IsMapEngineRunning()) RenderMap();
 pic.blit(x,y);
 FlipScreen();
 if (delay) Delay(delay);
 else {
  while (!GetKeypress()) {}
  }
 return pic;
 }

function ShowPic(img,x,y, delay) {
 RenderMap();
 img.blit(x,y);
 FlipScreen();
 Delay(delay);
 }

function ScrShot() {
 var tmp = GrabImage(0,0,W+32,W+32);
 return tmp;
 }

var gScrShot = 0;
function SaveScrShot() {
 var i = gScrShot;
 var fn = "";
 do {
  i = gScrShot;
  gScrShot++;
  fn = "scr"+pad(i,3,'0')+".png";
  } while (file_exists(fn,"images"));
 var s = ScrShot().createSurface();
 s.save(fn);
 if (flags.debug) {
  s.createImage().blit(0,0);
  ff.sys.drawText(0,H+32-ff.sys.getHeight(),"saved "+fn);
  FlipScreen();
  Delay(1000);
  }
 return true;
 }
function file_exists(fn,dn) {
 if (!dn) dn = "save";
 var dir = GetFileList(dn);
 for (var x=0; x<dir.length; x++) {
  if (dir[x]==fn) return true;
  }
 return false;
 }

function blitCenterTransform(img) {
 var w = GetScreenWidth();
 var h = GetScreenHeight();
 img.transformBlit(0,0, w,0, w,h, 0,h);
 }
function blitCenter(img) {
 var midH = GetScreenWidth()/2;
 var midV = GetScreenHeight()/2;
 img.blit(midH-img.width/2,midV-img.height/2);
 }
function blitCenterH(img,y) {
 var midS = GetScreenWidth()/2;
 img.blit(midS-img.width/2,y);
 }
function blitCenterV(img,x) {
 var midS = GetScreenHeight()/2;
 img.blit(x,midS-img.height/2);
 }
function blitCenterTransform(img) {
 img.transformBlit(0,0, W+32,0, W+32,H+32, 0,H+32);
 }
function blitCenterTransformMask(img,mask) {
 img.transformBlitMask(0,0, W+32,0, W+32,H+32, 0,H+32, mask);
 }
function blitCenterTransformMatrix(img,cm) {
 var s = img.createSurface();
 s.rescale(W+32,H+32);
 s.applyColorFX(0,0, s.width,s.height, cm);
 blitCenter(s.createImage());
 }

function GetClick() {
 if (IsMouseButtonPressed(MOUSE_LEFT)||
	 IsMouseButtonPressed(MOUSE_MIDDLE)||
	 IsMouseButtonPressed(MOUSE_RIGHT))
  return true;
 return false;
 }
function TriPointer() {
 var s = CreateSurface(16,16,CreateColor(0,0,0,0));
 var blk = CreateColor(32,32,32,255);
 var wh = CreateColor(224,192,0,255);
 for (var i=0; i<16; i++) {
  s.setPixel(i,0, blk);		// TOP LINE
  s.setPixel(0,i, blk);		// SIDE LINE
  }
 for (var x=1; x<5; x++) {
  for (var y=x; y<(7-(x-1)); y++) { // FILL WHITE
   s.setPixel(x,y, wh);		// LEFT
   s.setPixel(y,x, wh);		//
   s.setPixel(x,7+y, wh);	// BOTTOM
   s.setPixel(7+x,y, wh);	//
   s.setPixel(7+y,x, wh);	// RIGHT
   s.setPixel(y,7+x, wh);	//
   }
  }
 for (i=0; i<8; i++) {
  s.setPixel(7,i, blk);		// CENTER LINE
  s.setPixel(i,7, blk);		// MID LINE
  s.setPixel(7-i,i, blk);	// RIGHT -> LEFT DIAGONAL
  s.setPixel(i,7-i, blk);	// LEFT -> RIGHT DIAGONAL
  s.setPixel(15-i,i, blk);	// RIGHT -> LEFT BIG DIAGONAL
  s.setPixel(i,15-i, blk);	// LEFT -> RIGHT BIG DIAGONAL
  }
 //s.save("tri_sm.png");
 return s.createImage();
 }

const RED = 0;
const GREEN = 1;
const BLUE = 2;
const CYAN = 3;
const MAGENTA = 4;
const YELLOW = 5;
function nrgbar(valMin,valMax, baseHt,baseW,left,top, baseCol,hiCol, vert) {
 var temp = valMin;
 valMin = Math.min(valMin,valMax);
 valMax = Math.max(temp,valMax);
 var ratio = parseInt(baseW*valMin/valMax);
 var hi = 255, mid = 255*ratio/baseW, lo = 0;
 var r1 = (baseCol==RED||baseCol==MAGENTA||baseCol==YELLOW)?hi:0;
 var r2 = (r1==hi)?hi:(hiCol==RED||hiCol==MAGENTA||hiCol==YELLOW)?mid:lo;
 var g1 = (baseCol==GREEN||baseCol==CYAN||baseCol==YELLOW)?hi:0;
 var g2 = (g1==hi)?hi:(hiCol==GREEN||hiCol==CYAN||hiCol==YELLOW)?mid:lo;
 var b1 = (baseCol==BLUE||baseCol==CYAN||baseCol==MAGENTA)?hi:0;
 var b2 = (b1==hi)?hi:(hiCol==BLUE||hiCol==CYAN||hiCol==MAGENTA)?mid:lo;
 var c1 = CreateColor(r1,g1,b1); var c2 = CreateColor(r2,g2,b2);
 if (vert) {
  Rectangle(left-1,top-1, baseHt+2,baseW+2, CreateColor(16,16,16));
  GradientRectangle(left,top, baseHt,ratio,c2,c2,c1,c1);
  }
 else {
  Rectangle(left-1,top-1, baseW+2,baseHt+2, CreateColor(16,16,16));
  GradientRectangle(left,top, ratio,baseHt,c1,c2,c2,c1);
  }
 }

// math functions
function deg2rad(deg) {
 return (deg*Math.PI/180.0);
 }
function rad2deg(rad) {
 return (rad*180.0/Math.PI);
 }

function fix(num,min,max) {
 return parseInt(limit(num,min,max));
 }
function limit(num,min,max) {
 var temp = min;
 if (min>max) {min = max; max = temp;}
 if (num>max) num = max;
 else if (num<min) num = min;
 return num;
 }

//ptInRect() - if pt1 is within bounds of pt2+rect
function ptInRect(x1,y1,x2,y2,w,h) {
 return (x1>=x2&&x1<=x2+w)&&(y1>=y2&&y1<=y2+h);
 }

function distancePt(x1,y1, x2,y2) {
 return Math.sqrt(Math.pow(x1-x2,2)+Math.pow(y1-y2,2));
 }

function midpt(pt1,pt2) {
 var lowpt = (pt1<pt2)?pt1:pt2;
 var hipt = (pt1<pt2)?pt2:pt1;
 return (lowpt+(hipt-lowpt)/2);
 }

// string functions
function GetKeypress() {
 return (IsAnyKeyPressed());
 }

function keyStr(key,shift) { // GetKeyString() w/conv
 var ret = '';
 switch (key) {
  case KEY_ESCAPE: ret = "ESC"; break;
  case KEY_F1: ret = "F1"; break;
  case KEY_F2: ret = "F2"; break;
  case KEY_F3: ret = "F3"; break;
  case KEY_F4: ret = "F4"; break;
  case KEY_F5: ret = "F5"; break;
  case KEY_F6: ret = "F6"; break;
  case KEY_F7: ret = "F7"; break;
  case KEY_F8: ret = "F8"; break;
  case KEY_F9: ret = "F9"; break;
  case KEY_F10: ret = "F10"; break;
  case KEY_F11: ret = "F11"; break;
  case KEY_F12: ret = "F12"; break;
  case KEY_SHIFT: ret = "SHIFT"; break;
  case KEY_BACKSPACE: ret = "BKSP"; break;
  case KEY_TAB: ret = "TAB"; break;
  case KEY_CTRL: ret = "CTRL"; break;
  case KEY_ALT: ret = "ALT"; break;
  case KEY_SPACE: ret = "SPACE"; break;
  case KEY_ENTER: ret = "ENTER"; break;
  case KEY_INSERT: ret = "INS"; break;
  case KEY_DELETE: ret = "DEL"; break;
  case KEY_HOME: ret = "HOME"; break;
  case KEY_END: ret = "END"; break;
  case KEY_PAGEUP: ret = "PGUP"; break;
  case KEY_PAGEDOWN: ret = "PGDN"; break;
  case KEY_UP: ret = ".U"; break;
  case KEY_RIGHT: ret = ".R"; break;
  case KEY_DOWN: ret = ".D"; break;
  case KEY_LEFT: ret = ".L"; break;
  default: ret = GetKeyString(key,shift);
  }
 return ret;
 }

function trim(str) {
 var ret = "";
 str = str.toString();
 var leftI = false, rightI = false;
 var lI = 0, rI = str.length-1, len = str.length;
 for (var i=0; i<str.length; i++) {
  if (!leftI) {
   if (IsWhitespace(str[i])) len -= 1;
   else {leftI = true; lI = i;}
   }
  if (!rightI) {
   if (IsWhitespace(str[str.length-1-i])) len -= 1;
   else {rightI = true; rI = str.length-1-i;}
   }
  if (leftI&&rightI) break;
  }
 ret = str.substr(lI,len);
 return ret;
 }

function pad(num,sp,str) {
 var ret = ""; sp = parseInt(sp);
 var l1 = num.toString().length; var x = sp-l1;
 if (str) str = str.toString()[0]+""; else str = ' ';
 if (x>0) for (var i=0; i<x; i++) ret += ""+str;
 ret += num;
 return ret;
 }

function sprintf(fmtStr,varArr) {
 var ret = fmtStr;		// basic sprintf
 var currArg = 2;
 var currI = 0;
 var argv = varArr;
 if (arguments<2) return false;
 for (var i=0; i<ret; i++) {
  if (currArg>argv.length) return false;
  if (ret.indexOf("%i")>=0) {
   currI=ret.indexOf("%i");
   ret = ret.slice(0,currI)+parseInt(argv[currArg-1]).toString()+ret.slice(currI+2);
   currArg++;
   }
  if (ret.indexOf("%s")>=0) {
   currI=ret.indexOf("%s");
   ret = ret.slice(0,currI)+argv[currArg-1].toString()+ret.slice(currI+2);
   currArg++;
   }
  }
 return ret;
 }

function stripDir(str) {
 var ret = str.toString();
 var i = ret.indexOf('/');
 if (i>=0) ret = ret.substr(i+1,ret.length-i-1);
 return ret;
 }
function getDir(str) {
 var ret = str.toString();
 var i = ret.lastIndexOf('/');
 if (i>=0) ret = ret.substr(0,i);
 return ret;
 }

function toLower(str) {
 /*
 var ret = "";
 var upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
 var lower = "abcdefghijklmnopqrstuvwxyz";
 for (var i=0; i<str.length; i++) {
  if (isAlpha(str[i])) {
   var j = upper.indexOf(str[i]);
   if (!(j>=0)) j = lower.indexOf(str[i]);
   ret += lower[j];
   }
  else ret += str[i];
  }
 return ret;
 */
 return str.toLowerCase();
 }

function toUpper(str) {
 /*
 var ret = "";
 var upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
 var lower = "abcdefghijklmnopqrstuvwxyz";
 for (var i=0; i<str.length; i++) {
  if (isAlpha(str[i])) {
   var j = lower.indexOf(str[i]);
   if (!(j>=0)) j = upper.indexOf(str[i]);
   ret += upper[j];
   }
  else ret += str[i];
  }
 return ret;
 */
 return str.toUpperCase();
 }

// fromCharCode()
//  we're only gonna handle 0-127
//if (!function_exists(fromCharCode))
function fromCharCode(code) {
 if (code<0||code>127) Abort("fromCharCode(): invalid character index "+code);
 return unescape("%"+parseInt(code,16));
 }

function blank() {
 }
