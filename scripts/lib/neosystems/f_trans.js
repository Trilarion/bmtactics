/********************************************************************************
RM2K3 style screen transitions (hopefully)

mosaic - randomly "pixelates" the screen (block size is predefined unless called directly)
 tPixelate - container function for mosaic helper functions
 tTileIn - helper function for mosaic:in transition
 tMosaicIn - randomized tTileIn
 tTileOut - helper function for mosaic:out transition
 tMosaicOut - randomized tTileOut
 tRotateTileOut - tiles rotate out
fade - basic fade (wrapper for FadeIn/Out())
swish - rotate fade (WIP's Swish() transition)
melt - vertical melting (Fenix's Melt() transition)
split - horizontal split melting (based on Melt())
iris - circle iris in/out to/from color
wipe - wipe the screen
 wipeHx - wipe horizontally towards direction x (R, L, i=inward, o=outward)
 wipeVx - wipe vertically towards direction x (U, D, i=inward, o=outward)
 wipeDx - wipe diagonally towards corner x (1=UL, 2=UR, 3=LR, 4=LL)
********************************************************************************/
//ScrToSurface() - screenshot to a surface for manipulation
//ScrToImage() - ScrToSurface() to image for faster blitting
function ScrToSurface() {
 return GrabSurface(0,0, GetScreenWidth(),GetScreenHeight());
 }
function ScrToImage() {
 return ScrToSurface().createImage();
 }

//ScrTo2dArray() - screensurface to w*h portions in 2d array
//ScrToWidthArray() - screensurface to horizontal strips h thick
//ScrToHeightArray() - screensurface to vertical strips w thick
function ScrTo2dArray(w,h) {
 var SW = GetScreenWidth(), SH = GetScreenHeight();
 var s = ScrToSurface();
 var ret = new Array();
 var c = SW/w, r = SH/h;
 for (var i=0; i<r; i++) ret.push(new Array());
 for (i=0; i<ret.length; i++) {
  for (var j=0; j<c; j++) ret[i].push(s.cloneSection(j*w,i*h,w,h).createImage());
  }
 return ret;
 }
function ScrToWidthArray(h) {
 var SW = GetScreenWidth(), SH = GetScreenHeight();
 var s = ScrToSurface();
 var ret = new Array(), r = SH/h;
 for (var i=0; i<r; i++) ret.push(s.cloneSection(0,i*h,SW,h).createImage());
 return ret;
 }
function ScrToHeightArray(w) {
 var SW = GetScreenWidth(), SH = GetScreenHeight();
 var s = ScrToSurface();
 var ret = new Array(), c = SW/w;
 for (var i=0; i<c; i++) ret.push(s.cloneSection(i*w,0,w,SH).createImage());
 return ret;
 }

function transition(typeStr, inOut, ms, col) {
 var baseImg = ScrShot();	// just in case
 var w = 20, h = 20;
 var randomized = true;
 switch (typeStr) {
  case "mosaic": tPixelate(inOut, w,h, col, randomized); break;
  case "fade":
   var fStr = (inOut=="In")?"From":"To";
   eval("Fade"+fStr+"Color("+ms+",col)"); break;
  case "iris": tIris(inOut, ms, col); break;
  case "swirl": tSwish(2, ms, col); break;
  case "wipeHR": tWipe(inOut, "H","R", ms, 3*w, col); break;
  case "wipeHL": tWipe(inOut, "H","L", ms, 3*w, col); break;
  case "wipeHi":
  case "wipeHo": break;
  case "wipeVU": tWipe(inOut, "V","U", ms, 3*h, col); break;
  case "wipeVD": tWipe(inOut, "V","D", ms, 3*h, col); break;
  case "wipeVi":
  case "wipeVo": break;
  case "wipeD1": tWipe(inOut, "D",1, ms, 100, col); break;
  case "wipeD2": tWipe(inOut, "D",2, ms, 100, col); break;
  case "wipeD3": tWipe(inOut, "D",3, ms, 100, col); break;
  case "wipeD4": tWipe(inOut, "D",4, ms, 100, col); break;
  }
 }
/********************************************************************************
MOSAIC FUNCTIONS
********************************************************************************/
function tPixelate(inOut, w,h, col, rnd) {
 // pixelate screen
 // inOut - "In"=pixelate to screen from col, "Out"=pixelate to col from screen
 // w/h - width/height of blocks
 // ms - time in ms
 if (!IsColor(col)) col = CreateColor(0,0,0);
 var img = ScrToImage();
 if (inOut=="Out") {
  if (rnd) tMosaicOut(w,h,col);
  else tTileOut(w,h,col);
  ApplyColorMask(col);
  img.blit(0,0);
  }
 else if (inOut=="In") {
  if (rnd) tMosaicIn(w,h,col);
  else tTileIn(w,h,col);
  }
 else return false;
 }
function tTileIn(w,h,cfrom) {
 var sTemp = s = ScrToImage();
 var sArr = ScrTo2dArray(w,h);
 s.blit(0,0);
 Rectangle(0,0,GetScreenWidth(),GetScreenHeight(),cfrom);
 sTemp = GrabImage(0,0,GetScreenWidth(),GetScreenHeight());
 var rows = sArr.length, cols = sArr[0].length;
 for (var r=0; r<rows; r++) {
  for (var c=0; c<cols; c++) {
   sTemp.blit(0,0);
   sArr[r][c].blit(c*w,r*h);
   sTemp = ScrToImage();
   FlipScreen();
   }
  }
 s.blit(0,0);
 }
function tTileOut(w,h,cto) {
 var sTemp = s = ScrToImage();
 var sArr = ScrTo2dArray(w,h);
 s.blit(0,0);
 sTemp = GrabImage(0,0,GetScreenWidth(),GetScreenHeight());
 var rows = sArr.length, cols = sArr[0].length;
 for (var r=0; r<rows; r++) {
  for (var c=0; c<cols; c++) {
   sTemp.blit(0,0);
   Rectangle(c*w,r*h,w,h,cto);
   sTemp = ScrToImage();
   FlipScreen();
   }
  }
 s.blit(0,0);
 }
function tMosaicIn(w,h,cfrom) {
 var rate = 2;
 var sTemp = s = ScrToImage();
 var sArr = ScrTo2dArray(w,h);
 s.blit(0,0);
 Rectangle(0,0,GetScreenWidth(),GetScreenHeight(),cfrom);
 sTemp = GrabImage(0,0,GetScreenWidth(),GetScreenHeight());
 var rows = sArr.length, cols = sArr[0].length;
 var sl = rows*cols;
 var bArr = new Array(), rndChk = new Array();
 for (var i=0; i<rows; i++) {
  bArr.push(new Array());
  rndChk.push(false);
  for (var j=0; j<cols; j++) bArr[i].push(false);
  }
 var broken = false;
 var r = 0, c = 0, count = 0;
 do {
  sTemp.blit(0,0);
  for (i=0; i<bArr.length; i++) {
   if (!bArr[i].IsInArray(false)) rndChk[i] = true;
   }
  if (!rndChk.IsInArray(false)) broken = true;
  if (broken) break;
  do {
   r = parseInt(Math.random()*rows);
   c = parseInt(Math.random()*cols);
   } while (bArr[r][c]);
  bArr[r][c] = true;
  sArr[r][c].blit(c*w,r*h);
  sTemp = ScrToImage();
  if (count++%rate) FlipScreen();
  } while (!broken);
 s.blit(0,0);
 }
function tMosaicOut(w,h,cto) {
 var rate = 2;
 var sTemp = s = ScrToImage();
 var sArr = ScrTo2dArray(w,h);
 s.blit(0,0);
 var rows = sArr.length, cols = sArr[0].length;
 var sl = rows*cols;
 var bArr = new Array(), rndChk = new Array();
 for (var i=0; i<rows; i++) {
  bArr.push(new Array());
  rndChk.push(false);
  for (var j=0; j<cols; j++) bArr[i].push(false);
  }
 var broken = false;
 var r = 0, c = 0, count = 0;
 do {
  sTemp.blit(0,0);
  for (i=0; i<bArr.length; i++) {
   if (!bArr[i].IsInArray(false)) rndChk[i] = true;
   }
  if (!rndChk.IsInArray(false)) broken = true;
  if (broken) break;
  do {
   r = parseInt(Math.random()*rows);
   c = parseInt(Math.random()*cols);
   } while (bArr[r][c]);
  bArr[r][c] = true;
  Rectangle(c*w,r*h,w,h,cto);
  sTemp = ScrToImage();
  if (count++%rate) FlipScreen();
  } while (!broken);
 s.blit(0,0);
 ApplyColorMask(cto);
 s.blit(0,0);
 }
function tRotateTileOut(w,h,cto,dir) {
 var s = ScrToImage();
 var sArr = sArrTemp = sArrTemp2 = ScrTo2dArray(w,h);
 FlipScreen();
 //var sTemp = ScrToImage();
 var rows = sArr.length, cols = sArr[0].length;
 var b = (dir)?-1:1;
 var a = 0, astep = (cto.alpha)?cto.alpha:255;
 var dstep = 5, sstep = 100/(1440*dstep), m = 0;
 for (var i=0; i<360; i+=dstep) {
  if (m*sstep>=1) break;
  var fact = 1-m*sstep;
  for (var r=0; r<rows; r++) {
   for (var c=0; c<cols; c++) {
    var tSur = sArr[r][c].createSurface();
    tSur.rescale(fact*w,fact*h);
    tSur.createImage().rotateBlit(c*w,r*h,deg2rad(b*i));
    ApplyColorMask(CreateColor(cto.red,cto.green,cto.blue,a));
    }
   }
  FlipScreen();
  m++;
  // = (b*i)/astep;
  }
 ApplyColorMask(cto);
 s.blit(0,0);
 }
/********************************************************************************
END MOSAIC FUNCTIONS
********************************************************************************/
/********************************************************************************
WIPE FUNCTIONS

tWipe - wipe transition wrapper
tWipeIn - wipe:in wrapper
 tWipeInH - wipe:in.horizontal helper function
 tWipeInV - wipe:in.vertical helper function
 tWipeInD - wipe:in.vertical helper function
tWipeOut - wipe:out wrapper
 tWipeOutH - wipe:out.horizontal helper function
 tWipeOutV - wipe:out.vertical helper function
 tWipeOutD - wipe:out.diagonal helper function
********************************************************************************/
function tWipe(inOut, hv,dir, ms, size, col) {
 // wipe screen
 if (!IsColor(col)) col = Black;
 var img = ScrToImage();
 if (inOut=="Out") {
  tWipeOut(hv,dir,ms,size,col);
  }
 else if (inOut=="In") {
  tWipeIn(hv,dir,ms,size,col);
  }
 else return false;
 }
function tWipeOut(hv,dir, ms, size, col) {
 if (hv=="H") tWipeOutH(dir, ms, size, col);
 else if (hv=="V") tWipeOutV(dir, ms, size, col);
 else if (hv=="D") tWipeOutD(dir, ms, col);
 else return false;
 }
//tWipeOutH() - dir = L|R, ms >= 0, size >= 0, col is sphere.colorobj()
function tWipeOutH(dir, ms, size, col) {
 var op1 = 255, op2 = 0;
 var x = 0, w = rate = GetScreenWidth(), y = 0, h = GetScreenHeight();
 var w2 = size;
 var c1 = CreateColor(col.red,col.green,col.blue,op1);
 var c2 = CreateColor(col.red,col.green,col.blue,op2);
 if (ms>0) {
  var step = rate/ms;
  var w1 = 0, h1 = h;
  var time = GetTime();
  var time2 = GetTime();
  while (time2-time<ms) {
   img.blit(0,0);
   w1 = (time2-time)*step;
   if (dir=="R") {
    Rectangle(x,y, w1,h, c1);
    GradientRectangle(x+w1,y, w2,h, c1,c2,c2,c1);
    }
   else if (dir=="L") {
    Rectangle(w-w1,y, w1,h, c1);
    GradientRectangle(w-w1-w2,y, w2,h, c2,c1,c1,c2);
    }
   FlipScreen();
   time2 = GetTime();
   }
  }
 ApplyColorMask(c1);
 img.blit(0,0);
 }
//tWipeOutV() - dir = U|D, ms >= 0, size >= 0, col is sphere.colorobj()
function tWipeOutV(dir, ms, size, col) {
 var op1 = 255, op2 = 0;
 var x = 0, w = GetScreenWidth(), y = 0, h = rate = GetScreenHeight();
 var h2 = size;
 var c1 = CreateColor(col.red,col.green,col.blue,op1);
 var c2 = CreateColor(col.red,col.green,col.blue,op2);
 if (ms>0) {
  var step = rate/ms;
  var w1 = w, h1 = 0;
  var time = GetTime();
  var time2 = GetTime()
  while (time2-time<ms) {
   img.blit(0,0);
   h1 = (time2-time)*step;
   if (dir=="D") {
    Rectangle(x,y, w,h1, c1);
    GradientRectangle(x,y+h1, w,h2, c1,c1,c2,c2);
    }
   else if (dir=="U") {
    Rectangle(x,h-h1, w,h1, c1);
    GradientRectangle(x,h-h1-h2, w,h2, c2,c2,c1,c1);
    }
   FlipScreen();
   time2 = GetTime();
   }
  }
 ApplyColorMask(c1);
 img.blit(0,0);
 }
//tWipeOutD() - dir = 1|2|3|4, ms >= 0, size >= 0, col is sphere.colorobj()
//dir is in order ul, ur, lr, ll, like a gradient rectangle
function tWipeOutD(dir, ms, col) {
 if (dir<1||dir>4||!IsInt(dir)) dir = 1;
 var op1 = 255, op2 = 0;
 var x = 0, w = GetScreenWidth(), y = 0, h = GetScreenHeight();
 var M = Math.max(w,h), m = Math.min(w,h);
 var off = 5;
 var size = 100;
 var rate = Math.sqrt(w*w+h*h), d1 = w+h, d2 = M*Math.SQRT2;
 var sr = size*Math.SQRT2;
 var c1 = CreateColor(col.red,col.green,col.blue,op1);
 var c2 = CreateColor(col.red,col.green,col.blue,op2);
 var img = ScrToImage();
 var sFade = CreateSurface(size,d2,c2);
 sFade.setBlendMode(REPLACE);
 sFade.gradientRectangle(0,0,sFade.width,sFade.height,c1,c2,c2,c1);
 if (dir==2||dir==4) sFade.flipHorizontally();
 var iFade = sFade.createImage();
 if (ms>0) {
  var step = d1/ms;
  var s1 = 1;
  var time = GetTime();
  var time2 = GetTime()
  var maxW = false, maxH = false;
  var rw = 0, rh = 0, xc = 0, yc = 0;
  var x1 = y1 = 0, x2 = y2 = s1;
  while (time2-time<ms) {
   img.blit(0,0);
   s1 = (time2-time)*step;
   if (!maxW) {if (x2>w||x2<0) {maxW = true; htime = GetTime();}}
   else rh = (time2-htime)*step;
   if (!maxH) {if (y2>h||y2<0) {maxH = true; wtime = GetTime();}}
   else rw = (time2-wtime)*step;
   if (dir==1) {
    xc = x+rw+s1; yc = y+rh+s1;
    if (maxW) {Rectangle(x,y, w,rh,c1);}
    if (maxH) {Rectangle(x,y, rw,h,c1);}
    x1 = (maxH)?x+rw:x; x2 = (maxW)?w:(maxH)?x+rw+h:xc;
    y1 = (maxW)?y+rh:y; y2 = (maxH)?h:(maxW)?y+rw+w:yc;
    Triangle(x1,y1, x2,y1, x1,y2, c1)
    iFade.rotateBlit(x2-(iFade.width+off),y1-(iFade.width+off),Math.PI/4);
    }
   else if (dir==2) {
    xc = w-(x+rw+s1); yc = y+rh+s1;
    if (maxW) {Rectangle(x,y, w,rh,c1);}
    if (maxH) {Rectangle(w-rw,y, rw,h,c1);}
    x1 = (maxH)?w-rw:w; x2 = (maxW)?x:(maxH)?w-(rw+h):xc;
    y1 = (maxW)?y+rh:y; y2 = (maxH)?h:(maxW)?y+rw+w:yc;
    Triangle(x1,y1, x2,y1, x1,y2, c1);
    iFade.rotateBlit(x2+off,y1-(iFade.width+off),-Math.PI/4);
    }
   else if (dir==3) {
    xc = w-(x+rw+s1); yc = h-(y+rh+s1);
    if (maxW) {Rectangle(x,h-rh, w,rh,c1);}
    if (maxH) {Rectangle(w-rw,y, rw,h,c1);}
    x1 = (maxH)?w-rw:w; x2 = (maxW)?x:(maxH)?w-(rw+h):xc;
    y1 = (maxW)?h-rh:h; y2 = (maxH)?y:(maxW)?h-(rw+w):yc;
    Triangle(x1,y1, x2,y1, x1,y2, c1);
    iFade.rotateBlit(x2+off,y1-(2*iFade.width+sr),-Math.PI*3/4);
    }
   else if (dir==4) {
    xc = x+rw+s1; yc = h-(y+rh+s1);
    if (maxW) {Rectangle(x,h-rh, w,rh,c1);}
    if (maxH) {Rectangle(x,y, rw,h,c1);}
    x1 = (maxH)?x+rw:x; x2 = (maxW)?w:(maxH)?x+rw+h:xc;
    y1 = (maxW)?h-rh:h; y2 = (maxH)?y:(maxW)?h-(rw+w):yc;
    Triangle(x1,y1, x2,y1, x1,y2, c1)
    iFade.rotateBlit(x2-(iFade.width+off),y1-(2*iFade.width+sr),Math.PI*3/4);
    }
   FlipScreen();
   time2 = GetTime();
   if (rw>=w||rh>=h) break;
   }
  }
 ApplyColorMask(c1);
 img.blit(0,0);
 }
function tWipeIn(dir,col,rate) {
 ApplyColorMask(c1);
 img.blit(0,0);
 if (ms>0) {
  var step = rate/ms;
  var w1 = (hv=="H")?0:w, h1 = (hv=="H")?h:0;
  var time = GetTime();
  var time2 = GetTime()
  while (time2-time<ms) {
   img.blit(0,0);
   if (hv=="H") {
    w1 = W+32-((time2-time)*step);
    if (dir=="R") {
     Rectangle(W+32-w1,y, w1,h, c1);
     GradientRectangle(W+32-w1-w,y, w,h, c2,c1,c1,c2);
     }
    else {
     Rectangle(x,y, w1,h, c1);
     GradientRectangle(x+w1,y, w,h, c1,c2,c2,c1);
     }
    }
   else {
    h1 = H+32-((time2-time)*step);
    if (dir=="D") {
     Rectangle(x,H+32-h1, w,h1, c1);
     GradientRectangle(x,H+32-h1-h, w,h, c2,c2,c1,c1);
     }
    else {
     Rectangle(x,y, w,h1, c1);
     GradientRectangle(x,y+h1, w,h, c1,c1,c2,c2);
     }
    }
   FlipScreen();
   time2 = GetTime();
   }
  }
 ApplyColorMask(c2);
 img.blit(0,0);
 }
/********************************************************************************
END WIPE FUNCTIONS
********************************************************************************/
/********************************************************************************
IRIS FUNCTIONS
********************************************************************************/
function tIris(inOut, ms, col) {
 if (!IsColor(col)) col = Black;
 var img = ScrShot(), step;
 const w = GetScreenWidth(); const h = GetScreenHeight();
 var rMax = distancePt(w/2,h/2,0,0), rMin = 1;
 if (ms<=0) {
  ApplyColorMask(col);
  img.blit(0,0);
  return false;
  }
 step = (rMax-rMin)/ms;
 var delay = ms/(rMax-rMin);
 var lineC = LineCircle(rMax,col);
 if (inOut=="in") {
  var tmpImg = ScrShot();
  var rNow = rMax+delay;
  for (var rTmp=rMax; rTmp>rMin; rTmp-=delay) {
   var x1 = w/2-rMax, x2 = w/2-rTmp, x3 = w/2+rTmp;
   var y1 = h/2-rMax, y2 = h/2-rTmp, y3 = h/2+rTmp;
   tmpImg.blit(0,0);
   var fact = rTmp/rMax;
   Rectangle(x1,y1, w+rMax,y2-y1, col);
   Rectangle(x1,y3, w+rMax,y2-y1, col);
   Rectangle(x1,y1, x2-x1,h+rMax, col);
   Rectangle(x3,y1, x2-x1,h+rMax, col);
   for (var z=Math.abs(rNow-rTmp); z>0; z--) {
    lineC.zoomBlit(x2-z,y2-z,fact+z/rMax);
    //lineC.transformBlit(x2-z,y2-z,x2+2*rTmp+z,y2-z,x2+2*rTmp+z,y2+2*rTmp+z,x2-z,y2+2*rTmp+z);
    }
   lineC.zoomBlit(x2,y2,fact);
   tmpImg = ScrShot();
   FlipScreen();
   rNow -= delay;
   }
  ApplyColorMask(col);
  img.blit(0,0);
  }
 else if (inOut=="out") {
  //ApplyColorMask(col);
  img.blit(0,0);
  var rNow = rMin+delay;
  for (var rTmp=rMin; rTmp<rMax; rTmp+=delay) {
   var x1 = w/2-rMax, x2 = w/2-rTmp, x3 = w/2+rTmp;
   var y1 = h/2-rMax, y2 = h/2-rTmp, y3 = h/2+rTmp;
   img.blit(0,0);
   Rectangle(x1,y1, w+rMax,y2-y1, col);
   Rectangle(x1,y3, w+rMax,y2-y1, col);
   Rectangle(x1,y1, x2-x1,h+rMax, col);
   Rectangle(x3,y1, x2-x1,h+rMax, col);
   for (var z=0; z<distancePt(w/2+rTmp,h/2+rTmp, w/2+rNow,h/2+rNow)*2; z++) {
    var tmp = LineCircle(rTmp+z,col);
    tmp.blit(x2-z,y2-z);
    }
   FlipScreen();
   rNow += delay;
   }
  img.blit(0,0);
  }
 }
/********************************************************************************
END IRIS FUNCTIONS
********************************************************************************/
/********************************************************************************
SWISH, MELT
********************************************************************************/
function tSwish(speed, time, col) {
 var fps = GetFrameRate();
 SetFrameRate(10);
 var timer = GetTime();
 var deg = 0;
 var a = 0;
 var img = ScrToImage();
 while (timer + time > GetTime()) {
	img.blit(0, 0);
	img.rotateBlit(0, 0, deg2rad(deg));
	ApplyColorMask(CreateColor(col.red,col.green,col.blue,a));
	img = ScrToImage();
	FlipScreen();
	deg += speed/3;
	a += 1;
  }
 SetFrameRate(fps);
 }

function tMelt(w, fsp, sp, col, rnd) {
 if (!rnd) rnd = 0;
 if (!w||w<1) w = 1;
 var img = ScrToImage();
 var sArr = ScrToHeightArray(w), sLen = sArr.length;
 var arrSp = [], arrOff = [];
 for (var i=0; i<sLen; i++) {
  switch (rnd) {
   case 1: arrSp[i] = Math.random()*.6+1; break;
   case 2: arrSp[i] = i/sLen+1; break;
   case 3: arrSp[i] = (sLen-i)/sLen+1; break;
   case 4: arrSp[i] = Math.abs(sLen/2-i)/sLen+1; break;
   case 5: arrSp[i] = 2-Math.abs(sLen/2-i)/sLen; break;
   default: arrSp[i] = 1;
   }
  arrOff[i] = 0;
  }
 FlipScreen(); var done = false, a = 0;
 while (!done) {
  for (i=0; i<sLen; i++) {
   sArr[i].blit(i*w, arrOff[i]);
   arrOff[i] += sp*arrSp[i];
   }
  ApplyColorMask(CreateColor(col.red,col.green,col.blue,a));
  FlipScreen(); a += fsp;
  if (a>col.alpha) done = true;
  }
 ApplyColorMask(col);
 img.blit(0,0);
 }

function tSplit(h, fsp, sp, col, rnd) {
 if (!rnd) rnd = 0;
 if (!h||h<1) h = 1;
 var img = ScrToImage();
 var sArr = ScrToWidthArray(h), sLen = sArr.length;
 var arrSp = [], arrOff = [];
 for (var i=0; i<sLen; i++) {
  switch (rnd) {
   case 1: arrSp[i] = Math.random()*.6+1; break;
   case 2: arrSp[i] = i/sLen+1; break;
   case 3: arrSp[i] = (sLen-i)/sLen+1; break;
   case 4: arrSp[i] = Math.abs(sLen/2-i)/sLen+1; break;
   case 5: arrSp[i] = 2-Math.abs(sLen/2-i)/sLen; break;
   default: arrSp[i] = 1;
   }
  arrOff[i] = 0;
  }
 FlipScreen(); var done = false, a = 0;
 while (!done) {
  for (i=0; i<sLen; i++) {
   sArr[i].blit(arrOff[i], i*h);
   arrOff[i] += (i%2)?-sp*arrSp[i]:sp*arrSp[i];
   }
  ApplyColorMask(CreateColor(col.red,col.green,col.blue,a));
  FlipScreen();
  a += fsp;
  if (a>col.alpha) done = true;
  }
 ApplyColorMask(col);
 img.blit(0,0);
 }