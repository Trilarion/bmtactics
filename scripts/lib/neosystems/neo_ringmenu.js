// NeoLogiX RingMenu v1.0
// featuring trigonometric recalculation thanks to Fenix
// idea from Secret of Mana ring menu system
// intended as an alternative to standard grid-based menu systems
// - N e o L o g i X   (v1.0 c2004/02.21)
// added rotation animation, perspective
// - N e o L o g i X   (v1.1 c2004/02.27-02.28) (rotation, perspective)

/********************************************************************************
FEATURES:
 1. NRingMenu is a stylish alternative to your standard grid-based menu! Based
    off the ring menu system found in the Secret of Mana series, NRingMenu
    allows a Sphere programmer to organize menu objects in a circular
    arrangement instead of being confined to the usual top-down or left-right
    menu systems like flik_menu or the system menu.
 2. The positions of menu items in an NRingMenu are dynamically (re)calculated
    when the menu cursor increments/decrements. Animation of repositioning or
    opening may be added in the next update or two, but until then, be satisfied.
 2. Images or text can be used as menu items. Support will soon be added for
    spritesets & specialized text objects.
 3. Sound effects play when the cursor is moved, when an object is selected, and
    when the menu is escaped. These sounds can be set by the user but the default
    is no sound at all.
 4. NRingMenu's versatility allows for a variety of uses. Something as event-
    driven as an RM2K(3)-style choice menu or something as simple as an opening
    menu can be created using NRingMenu. Of course, if you want to go the
    "traditional" route, you can use it as a main menu.
 5. As a memory precaution, the engine wil abort if one of NRingMenu's methods is
    passed a faulty parameter (ie an index out of bounds).
********************************************************************************/

function NRingMenu(x,y,r,a) {
 if (!a) a = 0;
 if (r<1) r = 20;
 if (y<-r||y>GetScreenHeight()-r) y = r;
 if (x<-r||x>GetScreenWidth()-r) x = r;
 this.x = x;		// menu center x
 this.y = y;		// menu center y
 this.r = r;		// menu radius
 this.a = a;		// menu start angle (we don't like negative angles...)
 this.s = 0;		// angle difference/sector
 this.v = deg2rad(8);	// rotate velocity (every 8 degrees)
 this.p = 1.0;		// perspective

 this.arrow = GetSystemArrow();
 this.arrow_up = GetSystemUpArrow();
 this.arrow_down = GetSystemDownArrow();
 this.font = Font.system;
 this.bg = "";
 this.sound_move = "";
 this.sound_void = "";
 this.sound_enter = "";
 this.sound_cancel = "";
 this.selection = 0;
 this.done = false;
 this.items = new Array();
 this.keys = new Array();
 this.lastKey = "";

 this.wrapped = true;
 this.escapeable = function() {return false;}
 this.escape = function(isEntered) {
  if (!isEntered) isEntered = false;
  if (this.sound_move) {if (this.sound_move.isPlaying()) this.sound_move.stop();}
  if (this.sound_enter) {
   if (this.sound_enter.isPlaying()) this.sound_enter.stop();
   if (isEntered) this.sound_enter.play(false);
   }
  if (this.sound_cancel) {
   if (this.sound_cancel.isPlaying()) this.sound_cancel.stop();
   if (!isEntered) this.sound_cancel.play(false);
   }
  this.done = true;
  }
 this.defaultColorOff = CreateColor(160,160,160);
 this.defaultColorOn = White;
 this.disabledColor = CreateColor(96,96,96);
 }

// NRingMenu METHODS
NRingMenu.prototype.execute = function() {
 if (rad2deg(this.a)>=360) this.a = deg2rad(rad2deg(this.a)%360);
 while (this.a<0) this.a += 2*Math.PI;
 this.addDefaultKeys();
 do {
  this.render();
  this.handleKeys();
  } while (!this.done);
 }
NRingMenu.prototype.render = function() {
 if (this.bg) this.bg.blit(0,0);
 this.prerender();
 this.draw();
 this.postrender();
 FlipScreen();
 }
NRingMenu.prototype.defaultPrerender = function() {
 var l = this.items.length; var x = this.x, y = this.y;
 if (!l) return false;
 for (var i=0; i<l; i++) {
  var currA = this.s*((i-this.selection)%l)-this.a;
  var x = this.x+Math.cos(currA)*this.r;
  var y = this.y+Math.sin(currA)*this.r;
  if (this.p!=0) {
   y /= this.p; y += (this.y-this.r)/this.p;
   }
  GradientLine(this.x,this.y, x,y, Blue,White);
  }
 }
NRingMenu.prototype.prerender = NRingMenu.prototype.defaultPrerender;
NRingMenu.prototype.draw = function() {
 var currentfont = Font.system;
 var l = this.items.length; var x = this.x, y = this.y;
 if (!l) return false;
 for(var i=0; i<l; i++) {
  var curr = this.items[i];
  var currA = this.s*((i-this.selection)%l)-this.a;
  var x = this.x+Math.cos(currA)*this.r;
  var y = this.y+Math.sin(currA)*this.r;
  var yAlt = this.y+Math.sin(Math.PI+currA)*this.r;
  if (this.p!=0) {
   y /= this.p;
   yAlt /= this.p;
   }
  var pY = y;	// yAlt would allow for perspective to be larger at top
  //var pY = (this.a<Math.PI)?yAlt:y;
  var z = ((this.p-1)/5)*((2*(pY-this.r)/this.r))+1;
  if (this.p!=0) y += (this.y-this.r)/this.p;
  if (.99<z&&z<1.04) z = 1;
  var col = White;
  if (this.selection==i) {
   col = curr.hover;
   if (flags.debug) {
    //ff.txt.drawText(10,120,"selection="+this.selection);
    //ff.txt.drawText(10,120+ff.txt.getHeight()*1,"a="+parseInt(rad2deg(this.a)));
    //ff.txt.drawText(10,120+ff.txt.getHeight()*2,"a[selection]="+parseInt(rad2deg(currA)));
    //ff.txt.drawText(10,120+ff.txt.getHeight()*3,"x[selection]="+x);
    //ff.txt.drawText(10,120+ff.txt.getHeight()*4,"y[selection]="+y);
    //ff.txt.drawText(10,120+ff.txt.getHeight()*5,"z[selection]="+z);
    currentfont.drawText(10,120,"selection="+this.selection);
    currentfont.drawText(10,120+currentfont.getHeight()*1,"a="+parseInt(rad2deg(this.a)));
    currentfont.drawText(10,120+currentfont.getHeight()*2,"a[selection]="+parseInt(rad2deg(currA)));
    currentfont.drawText(10,120+currentfont.getHeight()*3,"x[selection]="+x);
    currentfont.drawText(10,120+currentfont.getHeight()*4,"y[selection]="+y);
    currentfont.drawText(10,120+currentfont.getHeight()*5,"z[selection]="+z);
    }
   }
  else col = curr.color;
  if (this.selection!=i)
   this.drawItem(i,x-this.getItemWidth(i)*z/2,y-this.getItemHeight(i)*z/2,col,z);
  }
 var xBase = this.x+Math.cos(this.a)*this.r;
 var yBase = this.y-Math.sin(this.a)*this.r;
 if (this.p!=0) {
  yBase /= this.p;
  }
 var z = ((this.p-1)/5)*((2*(yBase-this.r)/this.r))+1;
 if (this.p!=0) yBase += (this.y-this.r)/this.p;
 if (.99<z&&z<1.04) z = 1;
 this.drawItem(this.selection,
	xBase-this.getItemWidth(this.selection)*z/2,
	yBase-this.getItemHeight(this.selection)*z/2,
	this.items[this.selection].hover,z);
 }
NRingMenu.prototype.defaultPostrender = function() {
 // draw pointer at active item
 //Point(this.x,this.y, White);
 var xBase = this.x+Math.cos(this.a)*this.r;
 var yBase = this.y-Math.sin(this.a)*this.r;
 if (this.p!=0) {
  yBase /= this.p;
  }
 var z = ((this.p-1)/5)*((2*(yBase-this.r)/this.r))+1;
 if (this.p!=0) yBase += (this.y-this.r)/this.p;
 if (.99<z&&z<1.04) z = 1;
 this.arrow.blit(
	xBase-this.getItemWidth(this.selection)*z/2-this.arrow.width,
	yBase-this.getItemHeight(this.selection)*z/2);
 if (this.arrow_up)
  this.arrow_up.blit(
	xBase-this.arrow_up.width/2,
	yBase+this.getItemHeight(this.selection)*z/2);
 if (this.arrow_down)
  this.arrow_down.blit(
	xBase-this.arrow_down.width/2,
	yBase-this.getItemHeight(this.selection)*z/2-this.arrow_down.height);
 }
NRingMenu.prototype.postrender = NRingMenu.prototype.defaultPostrender;
NRingMenu.prototype.drawItem = function(i,x,y,c,z) {
 // draw item w/zoom factor z
 if (z<0) z = Math.abs(z);
 else if (!z) z = 1;
 var curr = this.items[i];
 switch (curr.type) {
  case "text":
   if (c&&c!=White) this.font.setColorMask(c);
   if (z!=1) this.font.drawZoomedText(x,y,z,curr.data.label);
   else this.font.drawText(x,y,curr.data.label);
   if (c) this.font.setColorMask(White);
   break;
  case "image":
   var s = curr.data.label.createSurface();
   s.rescale(curr.data.label.width*z,curr.data.label.height*z);
   var temp = s.createImage();
   if (c&&c!=White) temp.blitMask(x,y,c);
   else temp.blit(x,y); break;
  }
 }
NRingMenu.prototype.reset = function() {
 this.done = false;
 }
NRingMenu.prototype.handleSelection = function() {
 // execute action indexed by selection
 if (this.selection>=this.items.length||this.selection<0)
  Abort("handleSelection(): this.selection not between 0 & "+(this.items.length-1));
 var currSel = this.items[this.selection];
 if (currSel.isSelectable()) {
  currSel.action();
  }
 else if (this.sound_void) {
  if (this.sound_void.isPlaying()) this.sound_void.stop();
  this.sound_void.play(false);
  }
 return this.selection;
 }
NRingMenu.prototype.handleKeys = function() {
 // handle keypress actions
 if (this.keys.length==0) this.addDefaultKeys();
 for (var i=0; i<this.keys.length; i++) {
  if (BiosKeyPressed(this.keys[i].name)) {
   eval(this.keys[i].code);
   this.lastKey = this.keys[i].name;
   }
  }
 ClearKeyQueue();
 }

NRingMenu.prototype.width = function() {
 // gets ring's widest width
 var w = 0; var l = this.items.length;
 if (!l) return this.r*2;
 for (var i=0; i<l; i++) {
  if (this.getItemWidth(i)>w) w = this.getItemWidth(i);
  }
 return this.r*2+w;
 }
NRingMenu.prototype.height = function() {
 // gets ring's widest height
 var h = 0; var l = this.items.length;
 if (!l) return this.r*2;
 for (var i=0; i<l; i++) {
  if (this.getItemHeight(i)>h) h = this.getItemHeight(i);
  }
 return this.r*2+h;
 }
NRingMenu.prototype.getItemWidth = function(ndx) {
 if (ndx>=this.items.length||ndx<0)
  Abort("getItemWidth() ndx not between 0 & "+(this.items.length-1));
 var w = 0; var item = this.items[ndx];
 switch (item.type) {
  case "image": w = item.data.label.width; break;
  case "text": w = this.font.getStringWidth(item.data.label); break;
  }
 return w;
 }
NRingMenu.prototype.getItemHeight = function(ndx) {
 if (ndx>=this.items.length||ndx<0)
  Abort("getItemHeight() ndx "+ndx+" not between 0 & "+(this.items.length-1));
 var h = 0; var item = this.items[ndx];
 switch (item.type) {
  case "image": h = item.data.label.height; break;
  case "text": h = this.font.getHeight(); break;
  }
 return h;
 }


// NRingMenu MODIFIERS
NRingMenu.prototype.rotate = function(x) {
 // rotate menu by x*angledifference
 var oldA = this.a; var sign = (x<0)?-1:1;
 var endA = rad2deg(this.a+x*this.s)%360;	// in degrees for %math
 if (endA<=0) endA += 360;// if (sign==1) this.a += 2*Math.PI;
 var rEndA = deg2rad(endA);
 if (sign==-1) {
  while (this.a<rEndA) this.a += 2*Math.PI;
  do {
   this.render();
   this.a -= this.v;
   } while(this.a>rEndA);
  }
 else if (sign==1) {
  while (this.a>rEndA) this.a -= 2*Math.PI;
  do {
   this.render();
   this.a += this.v;
   } while(this.a<rEndA);
  }
 while (this.a>=2*Math.PI) this.a -= 2*Math.PI;
 return oldA;
 }
NRingMenu.prototype.addCurrentIndex = function(x) {
 // add x to selection (within allowed limits)
 if (this.sound_move) {
  if (this.sound_move.isPlaying()) this.sound_move.stop();
  this.sound_move.play(false);
  }
 var oldA = this.rotate(x);
 if (this.selection+x>=this.items.length) this.selection = 0;
 else if (this.selection+x<0) this.selection = this.items.length-1;
 else this.selection += x;
 this.a = oldA;
 if (this.selection>=this.items.length||this.selection<0)
  Abort("addCurrentIndex(): this.selection "+this.selection+"not between 0 & "+(this.items.length-1));
 }

NRingMenu.prototype.add = function(type,data,action,colorOff,colorOn,isDisabled) {
 // add an item to the menu
 var ndx = this.items.length;
 var selectable = function() {return true;}
 if (isDisabled) selectable = function() {return false;}
 if (!selectable()) {colorOn = this.disabledColor; colorOff = this.disabledColor;}
 else {
  if (!colorOn) colorOn = this.defaultColorOn;
  if (!colorOff) colorOff = this.defaultColorOff;
  }
 //Msgbox_new("calling add()...");
 this.items.push(new Object());
 this.items[ndx].type = type;
 this.items[ndx].data = data;
 this.items[ndx].action = action;
 this.items[ndx].color = colorOff;
 this.items[ndx].hover = colorOn;
 this.items[ndx].isSelectable = selectable;
 this.s = 2*Math.PI/this.items.length;
 }
NRingMenu.prototype.addText = function(txt,action,isDisabled, colorOff,colorOn) {
 // add text obj wrapper for add()
 if (!colorOff) colorOff = this.defaultColorOff;
 if (!colorOn) colorOn = this.defaultColorOn;
 var txtObj = new Object();
 txtObj.label = txt;
 this.add("text",txtObj,action, colorOff,colorOn,isDisabled);
 }
NRingMenu.prototype.addImage = function(img,action,isDisabled, colorOff,colorOn) {
 // add image obj wrapper for add()
 if (!colorOff) colorOff = this.defaultColorOff;
 if (!colorOn) colorOn = this.defaultColorOn;
 var imgObj = new Object();
 imgObj.label = (IsImage(img))?img:LoadImage(img);
 this.add("image",imgObj,action, colorOff,colorOn,isDisabled);
 }

NRingMenu.prototype.addDefaultKeys = function(act,esc,cw,ccw) {
 if (!ccw) ccw = KEY_LEFT;
 if (!cw) cw = KEY_RIGHT;
 if (!esc) esc = KEY_ESCAPE;
 if (!act) act = KEY_ENTER;
 this.addKey(cw,"this.addCurrentIndex(1)");
 this.addKey(ccw,"this.addCurrentIndex(-1)");
 this.addKey(act,"this.handleSelection()");
 this.addKey(esc,"if (this.escapeable()) this.escape()");
 }
NRingMenu.prototype.addKey = function(key,func) {
 // attach func to key in keys array 
 for (var i=0; i<this.keys.length; i++) {
  if (this.keys[i].name==key) {this.keys[i].code = func; return true;}
  }
 this.keys[this.keys.length] = new Object();
 this.keys[this.keys.length-1].name = key;
 this.keys[this.keys.length-1].code = func;
 return true;
 }

NRingMenu.prototype.setBG = function(img) {
 if (IsImage(img)) this.bg = img;
 else this.bg = LoadImage(img);
 }
NRingMenu.prototype.setCursor = function(img) {
 // set cursor image
 if (IsImage(img)) this.arrow = img;
 else this.arrow = LoadImage(img);
 }
NRingMenu.prototype.setArrows = function(u,d) {
 if (IsImage(u)) this.arrow_up = u;
 else this.arrow_up = LoadImage(u);
 if (IsImage(d)) this.arrow_down = d;
 else this.arrow_down = LoadImage(d);
 }
NRingMenu.prototype.setFont = function(f) {
 if (!IsFont(f)) f = LoadFont(f);
 this.font = f;
 }
NRingMenu.prototype.setSound = function(which,snd) {
 var s;
 if (IsSound(snd)) s = snd;
 else s = LoadSound(snd);
 switch (which) {
  case "move": this.sound_move = s; return this.sound_move; break;
  case "void": this.sound_void = s; return this.sound_void; break;
  case "enter": this.sound_enter = s; return this.sound_enter; break;
  case "cancel": this.sound_cancel = s; return this.sound_cancel; break;
  }
 return false;
 }
NRingMenu.prototype.setDefaultSounds = function(move,act,esc,buzz) {
 if (!buzz) buzz = "02_buzz.wav";
 if (!esc) esc = "03_shif.wav";
 if (!act) act = "01_ching.wav";
 if (!move) move = "00_drop.wav";
 this.setSound("move",move);
 this.setSound("void",buzz);
 this.setSound("enter",act);
 this.setSound("cancel",esc);
 }