/*******************************************************************************
Window(ws,f,img, xoff,yoff) - container object for a windowstyle and base font
 ws - windowstyle to use
 f - font to use
 img - image to use
 xoff - horizontal offset of actual text (to accomodate for the edges of the ws)
 yoff - vertical offset of actual text (to accomodate for the edge of the ws)

METHODS
setWindowStyle(ws) - set the window's windowstyle
setFont(f) - set the window's font
setImage(img) - set the window's image
drawText(x,y,txt) - draw text with windowstyle at once
drawText_new(x,y,txt) - draw text with windowstyle letter-by-letter
drawTextBox(x,y, w,h, off,txt) - draw textbox with windowstyle at once
drawTextBox_new(x,y, w,h, off, txt) - draw textbox with windowstyle letter-by-letter
drawMsgBox(x,y, txt,capt,lines) - draw full-on message box

PROPERTIES
ws - the windowstyle
font - the font
image - the image (1*1 means "no image offset")
offsetX - the window's x offset (to account for windowstyle)
offsetY - the window's y offset (to account for windowstyle)
a_down - window's down arrow
a_up - window's up arrow

TODO: make Window use Font instead of CFont
*******************************************************************************/
function Window(ws,f,img, xoff,yoff,col) {
 this.setWindowStyle(ws);
 this.setFont(f);
 this.setImage(img);
 this.offsetX = (xoff)?xoff:16;
 this.offsetY = (yoff)?yoff:16;
 this.a_down = arrow_down;
 this.a_up = arrow_up;
 this.font_capt = new Fontx("square1.rfn");
 if (col&&IsColor(col)) this.font_capt.setColorMask(col);
 }
Window.prototype.setWindowStyle = function(ws) {
 this.ws = (IsWindowStyle(ws))?ws:(IsString(ws))?LoadWindowStyle(ws):GetSystemWindowStyle();
 }
Window.prototype.setFont = function(f) {
 this.font = new Fontx(f);
 }
Window.prototype.setImage = function(img) {
 this.image = (IsImage(img))?img:(IsString(img))?LoadImage(img):GetBlankImage();
 }
Window.prototype.setArrow = function(img) {
 this.a_down = (IsImage(img))?img:arrow_down;
 }
Window.prototype.getHeight = function() {
 if (this.image.height>1) return this.image.height;
 else return 0;
 }
Window.prototype.getFontHeight = function() {
 return this.font.getHeight();
 }
Window.prototype.getArrow = function() {
 if (this.a_down) return this.a_down;
 }
Window.prototype.drawWindow = function(x,y,w,h,capt) {
 if (x<this.offsetX) x = this.offsetX;
 if (y<this.offsetY) y = this.offsetY;
 if (x+w>GetScreenWidth()-this.offsetX) w = GetScreenWidth()-x-this.offsetX;
 if (y+h>GetScreenHeight()-this.offsetY) h = GetScreenHeight()-y-this.offsetY;
 this.ws.drawWindow(x,y,w,h);
 var ch = this.font_capt.getHeight();
 if (capt) this.font_capt.drawText(x,y-ch,capt);
 }
Window.prototype.drawWindowedText = function(x,y, txt,align,capt, w,h) {
 if (!w||w<1) w = this.font.getStringWidth(txt);
 if (!h||h<this.font.getHeight()) h = this.font.getHeight();
 if (x<this.offsetX) x = this.offsetX;
 if (y<this.offsetY) y = this.offsetY;
 if (x+w>GetScreenWidth()-this.offsetX) w = GetScreenWidth()-x-this.offsetX;
 if (y+h>GetScreenHeight()-this.offsetY) h = GetScreenHeight()-y-this.offsetY;
 this.drawWindow(x,y,w,h,capt);
 switch (align.toLowerCase()) {
  case "right": this.font.drawText(x+w,y, txt,"right"); break;
  case "center": this.font.drawText(x+w/2,y, txt,"center"); break;
  default: this.font.drawText(x,y, txt); break;
  }
 }
// all of Window's text functions FlipScreen()
Window.prototype.drawText = function(x,y, txt) {
 var bg = ScrShot();
 if (x<this.offsetX) x = this.offsetX;
 if (y<this.offsetY) y = this.offsetY;
 var w = GetScreenWidth()-2*x;
 this.ws.drawWindow(x,y, w,this.font.getHeight());
 this.font.drawText(x,y, txt);
 var img = ScrShot();
 FlipScreen();
 var a = 0, dir = "up";
 while (!IsKeyPressed(k.cancel)) {
  img.blit(0,0);
  this.a_down.blitMask(w,y+this.font.getHeight(), CreateColor(255,255,255,a));
  FlipScreen();
  if (dir=="up") {
   if (a<255) a += 10;
   else dir = "down";
   }
  else if (dir=="down") {
   if (a>0) a -= 10;
   else dir = "up";
   }
  }
 ClearKeyQueue();
 bg.blit(0,0);
 }
Window.prototype.drawText_new = function(x,y, txt,capt) {
 var bg = ScrShot();
 if (x<this.offsetX) x = this.offsetX;
 if (y<this.offsetY) y = this.offsetY;
 var w = GetScreenWidth()-2*x;
 this.ws.drawWindow(x,y, w,this.font.getHeight());
 if (capt) this.font_capt.drawText(x,y-this.font_capt.getHeight(),capt);
 Drawtext_new(this.font, x,y, txt);
 var img = ScrShot();
 FlipScreen();
 var a = 0, dir = "up";
 while (!IsKeyPressed(k.cancel)) {
  img.blit(0,0);
  this.a_down.blitMask(w,y+this.font.getHeight(), CreateColor(255,255,255,a));
  FlipScreen();
  if (dir=="up") {
   if (a<255) a += 10;
   else dir = "down";
   }
  else if (dir=="down") {
   if (a>0) a -= 10;
   else dir = "up";
   }
  }
 ClearKeyQueue();
 bg.blit(0,0);
 }
Window.prototype.drawTextBox = function(x,y, w,h, off,txt,capt) {
 var bg = ScrShot();
 if (x<this.offsetX) x = this.offsetX;
 if (y<this.offsetY) y = this.offsetY;
 if (w>GetScreenWidth()-2*x) w = GetScreenWidth()-2*x;
 if (h>GetScreenHeight()-2*y) h = GetScreenHeight()-2*y;
 this.ws.drawWindow(x,y, w,h);
 if (capt) this.font_capt.drawText(x,y-this.font_capt.getHeight(),capt);
 this.font.drawTextBox(x,y, w,h, off, txt);
 var img = ScrShot();
 FlipScreen();
 var a = 0, dir = "up";
 while (!IsKeyPressed(k.cancel)) {
  img.blit(0,0);
  this.a_down.blitMask(w,y+this.font.getHeight(), CreateColor(255,255,255,a));
  FlipScreen();
  if (dir=="up") {
   if (a<255) a += 10;
   else dir = "down";
   }
  else if (dir=="down") {
   if (a>0) a -= 10;
   else dir = "up";
   }
  }
 ClearKeyQueue();
 bg.blit(0,0);
 }
Window.prototype.drawTextBox_new = function(x,y, w,lines, d, txt,capt) {
 var bg = ScrShot();
 if (x<this.offsetX) x = this.offsetX;
 if (y<this.offsetY) y = this.offsetY;
 if (w>GetScreenWidth()-2*x) w = GetScreenWidth()-2*x;
 if (h>GetScreenHeight()-2*y) h = GetScreenHeight()-2*y;
 this.ws.drawWindow(x,y, w,h);
 if (capt) this.font_capt.drawText(x,y-this.font_capt.getHeight(),capt);
 Drawtextbox_new(this.font, x,y, w,lines, d, txt);
 var img = ScrShot();
 FlipScreen();
 var a = 0, dir = "up";
 while (!IsKeyPressed(k.cancel)) {
  img.blit(0,0);
  arrow_down.blitMask(w,y+this.font.getHeight(), CreateColor(255,255,255,a));
  FlipScreen();
  if (dir=="up") {
   if (a<255) a += 10;
   else dir = "down";
   }
  else if (dir=="down") {
   if (a>0) a -= 10;
   else dir = "up";
   }
  }
 ClearKeyQueue();
 bg.blit(0,0);
 }
Window.prototype.drawMsgBox = function(x,y, txt,capt,lines) {
 var bg = ScrShot();
 if (x<this.offsetX) x = this.offsetX;
 if (y<this.offsetY) y = this.offsetY;
 var fh = this.font.getHeight(), ch = this.font_capt.getHeight();
 if (!lines||lines<1) lines = 1;
 var w = GetScreenWidth()-2*x; var h;
 while (y+(h = lines*fh)>GetScreenHeight()) lines--;
 if (h<this.image.height) h = this.image.height;
 var w2 = w, x2 = x;
 if (this.image.width>1) {
  w2 -= this.image.width;
  x2 += this.image.width;
  }

 this.ws.drawWindow(x,y, w,h);
 this.image.blit(x,y);
 if (capt) this.font_capt.drawText(x,y-ch,capt);
 Drawtextbox_new(this.font,x2,y, w2,lines, 500,txt);
 ClearKeyQueue();
 var img = ScrShot();
 FlipScreen();
 var a = 0, dir = "up";
 while (!IsKeyPressed(k.cancel)) {
  img.blit(0,0);
  this.a_down.blitMask(x+w,y+h-this.a_down.height, CreateColor(255,255,255,a));
  FlipScreen();
  if (dir=="up") {
   if (a<255) a += 10;
   else dir = "down";
   }
  else if (dir=="down") {
   if (a>0) a -= 10;
   else dir = "up";
   }
  }
 ClearKeyQueue();
 bg.blit(0,0);
 }
Window.prototype.drawMsgScroll = function(x,y, txt,capt,lines) {
 var bg = ScrShot();
 if (x<this.offsetX) x = this.offsetX;
 if (y<this.offsetY) y = this.offsetY;
 var fh = this.font.getHeight(), ch = this.font_capt.getHeight();
 if (!lines||lines<1) lines = 1;
 var w = GetScreenWidth()-2*x; var h;
 while (y+(h = lines*fh)>GetScreenHeight()) lines--;
 if (h<this.image.height) h = this.image.height;
 var w2 = w, x2 = x;
 if (this.image.width>1) {
  w2 -= this.image.width;
  x2 += this.image.width;
  }

 this.ws.drawWindow(x,y, w,h);
 this.image.blit(x,y);
 if (capt) this.font_capt.drawText(x,y-ch,capt);
 Drawtextbox_scroll(this.font,x2,y, w2,lines, 500,txt);
 ClearKeyQueue();
 var img = ScrShot();
 FlipScreen();
 bg.blit(0,0);
 }
 
 /*******************************************************************************
Font(f,col) - container object for a base font
 f - font to use
 col - color to apply

METHODS
setFont(f) - set the font
setColorMask(col) - set the font's color
getColorMask()
getHeight()
getStringWidth()
getStringHeight()
drawText(x,y,txt,halign, valign) - draw text aligned
drawText_new(x,y,txt,halign,valign,start) -
 draw text aligned letter-by-letter starting from start
drawZoomedText()
drawTextBox()
draw()
clone()
addBreaks(txt, w) - break text w/hard line breaks
input(x,y, len) - input text at [x,y] w/max length len

PROPERTIES
font - the font
color - the font's color
*******************************************************************************/
function Fontx(f,col) {
 this.setFont(f);
 if (col&&col.alpha!=0) this.setColorMask(col);
 else this.setColorMask();
 }
Fontx.prototype.setFont = function(f) {
 this.font = (IsFont(f))?f:LoadFont(f);
 }
Fontx.prototype.setColorMask = function(col) {
 this.color = (IsColor(col))?col:CreateColor(255,255,255);
 }
Fontx.prototype.getColorMask = function() {
 if (this.color) return this.color;
 else return CreateColor(255,255,255);
 }
Fontx.prototype.getHeight = function() {
 return this.font.getHeight();
 }
Fontx.prototype.getStringWidth = function(txt) {
 return this.font.getStringWidth(txt);
 }
Fontx.prototype.getStringHeight = function(txt,w) {
 if (!w||w<1) w = this.font.getStringWidth(txt);
 return this.font.getStringHeight(txt,w);
 }
Fontx.prototype.getSmallCharWidth = function() {
 var w = GetScreenWidth();
 for (var i=32; i<128; i++) {
  var fw = this.font.getStringWidth(fromCharCode(i));
  if (fw<w) w = fw;
  }
 return w;
 }
Fontx.prototype.getLargeCharWidth = function() {
 var w = 0;
 for (var i=32; i<128; i++) {
  var fw = this.font.getStringWidth(fromCharCode(i));
  if (fw>w) w = fw;
  }
 return w;
 }
Fontx.prototype.drawText = function(x,y, txt, halign,valign) {
 var xoff = 0, yoff = 0;
 if (!halign) halign = "left";
 if (!valign) valign = "top";
 switch (halign.toLowerCase()) {
  case "right": xoff = this.font.getStringWidth(txt); break;
  case "center": xoff = this.font.getStringWidth(txt)/2; break;
  default: xoff = 0;
  }
 switch (valign.toLowerCase()) {
  case "bottom": yoff = this.font.getHeight(); break;
  case "middle": yoff = this.font.getHeight()/2; break;
  default: yoff = 0;
  }
 var oldC = this.font.getColorMask();
 if (this.color&&this.color.alpha!=0) this.font.setColorMask(this.color);
 this.font.drawText(x-xoff,y-yoff,txt);
 this.font.setColorMask(oldC);
 }
Fontx.prototype.drawText_new = function(x,y, txt, halign,valign) {
 var xoff = 0, yoff = 0;
 switch (halign) {
  case "right": xoff = this.font.getStringWidth(txt); break;
  case "center": xoff = this.font.getStringWidth(txt)/2; break;
  default: xoff = 0;
  }
 switch (valign) {
  case "bottom": yoff = this.font.getHeight(); break;
  case "middle": yoff = this.font.getHeight()/2; break;
  default: yoff = 0;
  }
 var oldC = this.font.getColorMask();
 if (this.color.alpha!=0) this.font.setColorMask(this.color);
 Drawtext_new(this.font, x-xoff,y-yoff,txt);
 this.font.setColorMask(oldC);
 }
Fontx.prototype.drawZoomedText = function(x,y,z, txt) {
 this.font.drawZoomedText(x,y,z,txt);
 }
Fontx.prototype.drawTextBox = function(x,y,w,h,off,txt,align) {
 if (!align) align = "left";
 switch (align.toLowerCase()) {
  case "center": case "right": align = align.toLowerCase(); break;
  default: align = "left";
  }
 var t = this.addBreaks(txt,w);
 if (align=="left") {
  this.font.drawTextBox(x,y,w,h,off,t);
  return;
  }
 var a = t.split("\n");
 var xoff = 0;
 if (align=="center") xoff = w/2;
 else if (align=="right") xoff = w;
 else xoff = 0;
 var r = GetClippingRectangle();
 //SetClippingRectangle(x,y,w,h);
 for (var i=0; i<a.length; i++)
  this.drawText(x+xoff,y+off+i*this.getHeight(), a[i], align);
 SetClippingRectangle(r.x,r.y,r.width,r.height);
 }
Fontx.prototype.draw = function(x,y,txt) {
 var w = this.getStringWidth(txt);
 if (x+w>GetScreenWidth()) w = GetScreenWidth()-x;
 this.font.drawTextBox(x,y,w,this.getStringHeight(txt,w),0,txt);
 }
Fontx.prototype.clone = function() {
 var f = this.font;
 var c = (this.color)?this.color:undefined;
 var ret = new Font(f,c);
 return ret;
 }
// addBreaks -
//  attempts to break txt w/newlines according to w
//  we don't like tiny or too large w values, so
//  we limit w to between the width of the smallest
//  displayable char (not including \t or \n) and
//  the width of the screen; if we put an x param
//  w would take into account x as well
// the how:
//  for() loop thru txt, temp word adds next char
//  if char isn't space, tab, or newline; if
//  total line width > w, add temp word to ret
//  then add newline, then clear temp word;
//  else add next char to temp word regardless
Fontx.prototype.addBreaks = function(txt, w) {
 if (w<this.getSmallCharWidth()) w = this.getSmallCharWidth();
 if (w>GetScreenWidth()) w = GetScreenWidth();
 var ret = "", dx = 0, w_wd = 0, word = "";
 for (var i=0; i<txt.length; i++) {	// fix loop to handle line-by-line if needed
  var ch = txt.charAt(i);
  var w_ch = this.getStringWidth(ch);
  switch (ch) {
   case "\n": ret += word;
    dx = 0; ret += ch;
    word = ""; w_wd = 0;
    break;
   case " ": case "\t":
    ret += word;
    if (dx+w_wd+w_ch>w) {dx = w_wd+w_ch; ret += "\n";}
    else {dx += w_wd+w_ch; ret += ch;}
    word = ""; w_wd = 0;
    break;
   default:
    if (w_wd+w_ch>w&&dx==0) {ret += word; ret += "\n"; word = ""; w_wd = 0;}
    else if (dx+w_wd+w_ch>w) {dx = 0; ret += "\n";}
    word += ch; w_wd += w_ch;
    break;
   }
  }
 if (word) ret += word;
 return ret;
 }
Fontx.prototype.input = function(x,y, len) {
 var bg = ScrShot();
 var cursor = CreateSurface(2,this.getHeight(),this.getColorMask()).createImage();
 var str = "";
 var pos = 0;
 // No max given, default to 256
 if (!IsInt(len)) max_chars = 256;
 while (true) {
  bg.blit(0, 0);
  this.drawText(x,y, str);
  if (Math.sin(GetTime()>>8)>0)
   cursor.blit(x+this.getStringWidth(str.slice(0,pos)), y);
  FlipScreen();
  while (AreKeysLeft()) {
   var key = GetKey();
   switch (key) {
    case KEY_ENTER: return str; break;
    case KEY_BACKSPACE:
     if (str!="") str = str.slice(0, pos-1) + str.slice(pos);
     if (pos>0) pos--;
     break;
    case KEY_DELETE: if (str!="") str = str.slice(0, pos) + str.slice(pos+1); break;
    case KEY_LEFT: if (pos>0) pos--; break;
    case KEY_RIGHT:
     if (pos<=str.length-1) pos++; break;
    case KEY_HOME: pos = 0; break;
    case KEY_END: pos = str.length; break;
    default:
     var shift = IsKeyPressed(KEY_SHIFT);
     if (GetKeyString(key, shift) != "" && (str.length < len)) {
      str = str.slice(0, pos) + GetKeyString(key, shift) + str.slice(pos);
      pos++;
      }
    } // end switch
   } // end while (keys left)
  } // end while (true)
 }

/*******************************************************************************
Image(img) - container object for an img
 img - img to use

METHODS
setImage(img) - set the image
getHeight()
getWidth()
blit()
blitMask()
rotateBlit()
rotateBlitMask()
zoomBlit()
zoomBlitMask()
transformBlit()
transformBlitMask()
createSurface()
createMapArea(x,y, func)
blitMap(x,y)
blitMapRender(x,y)
clone()

PROPERTIES
img - the image
width - image's width
height - image's height
area[] - imagemap area array
*******************************************************************************/
function Image(img) {
 this.setImage(img);
 this.width = this.img.width;
 this.height = this.img.height;
 this.area = [];
 }
Image.prototype.setImage = function(img) {
 this.img = (IsImage(img))?img:LoadImage(img);
 }
Image.prototype.getHeight = function() {
 if (this.height&&IsInt(this.height)) return this.height;
 if (this.img.height) return this.img.height;
 return 0;
 }
Image.prototype.getWidth = function() {
 if (this.width&&IsInt(this.width)) return this.width;
 if (this.img.width) return this.img.width;
 return 0;
 }
Image.prototype.blit = function(x,y) {
 this.img.blit(x,y);
 }
Image.prototype.blitMask = function(x,y, col) {
 this.img.blitMask(x,y, col);
 }
Image.prototype.rotateBlit = function(x,y, r) {
 this.img.rotateBlit(x,y, r);
 }
Image.prototype.rotateBlitMask = function(x,y, r, col) {
 this.img.rotateBlitMask(x,y, r, col);
 }
Image.prototype.zoomBlit = function(x,y,z) {
 this.img.zoomBlit(x,y,z);
 }
Image.prototype.zoomBlitMask = function(x,y,z, col) {
 this.img.zoomBlitMask(x,y,z, col);
 }
Image.prototype.transformBlit = function(x1,y1, x2,y2, x3,y3, x4,y4) {
 this.img.transformBlit(x1,y1, x2,y2, x3,y3, x4,y4);
 }
Image.prototype.transformBlitMask = function(x1,y1, x2,y2, x3,y3, x4,y4, col) {
 this.img.transformBlitMask(x1,y1, x2,y2, x3,y3, x4,y4, col);
 }
Image.prototype.createSurface = function() {
 return this.img.createSurface();
 }
Image.prototype.createMapArea = function(x,y, w,h, func, mouse) {
 if (!IsInt(x)||!IsInt(y)||!IsInt(w)||!IsInt(h)) Abort("can't create imagemap area");
 Assert("w>0"); Assert("h>0");
 if (!mouse||(mouse!=MOUSE_LEFT&&mouse!=MOUSE_MIDDLE&&mouse!=MOUSE_RIGHT)) mouse = MOUSE_LEFT;
 var o = new Object();
 o.x = x; o.y = y;
 o.w = w; o.h = h;
 o.action = func; o.button = mouse;
 this.area.push(o);
 }
// use blitMap() for an engine stalling maparea get
// use blitMapRender() in a renderscript
Image.prototype.blitMap = function(x,y) {
 var bg = ScrShot();
 var clicked = false;
 var mX = 0, mY = 0, ret = 0;
 do {
  bg.blit(0,0);
  this.img.blit(x,y);
  mX = GetMouseX();
  mY = GetMouseY();
  for (var i=0; i<this.area.length; i++) {
   var a = this.area[i];
   if (ptInRect(mX,mY, a.x,a.y, a.w,a.h)&&IsMouseButtonPressed(a.button)) {
    a.action(); clicked = true; ret = i; break;
    }
   }
  } while(!clicked);
 return ret;
 }
Image.prototype.blitMapRender = function(x,y) {
 var mX = GetMouseX();
 var mY = GetMouseY();
 for (var i=0; i<this.area.length; i++) {
  var a = this.area[i];
  if (ptInRect(mX,mY, a.x,a.y, a.w,a.h)&&IsMouseButtonPressed(a.button)) {
   a.action(); return i;
   }
  }
 }
Image.prototype.clone = function() {
 var tmp = this.img;
 var ret = new Image(tmp);
 return ret;
 }

if (!function_exists(ptInRect))
function ptInRect(x1,y1, x2,y2, w,h) {
 return (x1>=x2&&x1<=x2+w)&&(y1>=y2&&y1<=y2+h);
 }
