// Betrayer's Moon Jukebox Source!! DREMELOFDEATH 2005 UNDER GPL
function JukeboxService(volume) {
	this.stream = true;
	this.CurrentlyPlaying = undefined;
	this.BGM = LoadSound("void.wav",this.stream);
	this.BGM2 = LoadSound("void.wav",this.stream);
	this.RepeatSave = false;
	this.DefaultVolume = volume;
	this.CrossfadeUpper = false;
}
function Musickify(what) {
	return "music/" + what;
}
JukeboxService.prototype.LoadPlayBGM = function(what,repeat) {
	what = Musickify(what);
	this.CurrentlyPlaying = what;
	this.CrossfadeUpper = false;
	this.BGM = LoadSound(what,this.stream);
	this.BGM.setVolume(this.DefaultVolume);
	this.RepeatSave = repeat;
	this.BGM.setVolume(this.DefaultVolume);
	this.BGM.play(repeat);
}
JukeboxService.prototype.StopBGM = function() {
	this.CurrentlyPlaying = undefined;
	if(!this.CrossfadeUpper) {this.BGM.stop();} else {this.BGM2.stop();}
	this.CrossfadeUpper = false;
}
JukeboxService.prototype.PauseBGM = function() {
	if(!this.CrossfadeUpper) {this.BGM.pause();} else {this.BGM2.pause();}
}
JukeboxService.prototype.ResumeBGM = function() {
	if(!this.CrossfadeUpper) {this.BGM.play(this.RepeatSave);} else {this.BGM2.play(this.RepeatSave);}
}
JukeboxService.prototype.FadeToLevelBGM = function(to, speed) {
	var vol = undefined;
	if(!this.CrossfadeUpper) {vol = this.BGM.getVolume();} else {vol = this.BGM2.getVolume();}
	var asdf = false;
	if(to > vol) asdf = true;
	while(1) {
		if(!this.CrossfadeUpper) {
			if((asdf && this.BGM.getVolume() >= to) || (!asdf && this.BGM.getVolume() <= to)) break;
		} else {
			if((asdf && this.BGM2.getVolume() >= to) || (!asdf && this.BGM2.getVolume() <= to)) break;
		}
		if(asdf) {vol += speed;} else {vol -= speed;}
		if((asdf && vol > to) || (!asdf && vol < to)) {vol = to;}
		if(!this.CrossfadeUpper) {this.BGM.setVolume(vol);} else {this.BGM2.setVolume(vol);}
		wait(1);
	}
}
JukeboxService.prototype.FadeOutBGM = function(speed) {
	this.FadeToLevelBGM(0,speed);
	this.StopBGM();
}
JukeboxService.prototype.FadeInAndLoadPlayBGM = function(what,repeat,speed) {
	what = Musickify(what);
	this.CurrentlyPlaying = what;
	this.CrossfadeUpper = false;
	this.BGM = LoadSound(what,this.stream);
	this.RepeatSave = repeat;
	this.BGM.play(repeat);
	this.BGM.setVolume(0);
	this.FadeToLevelBGM(this.DefaultVolume,speed);
}
JukeboxService.prototype.LoadCrossfadeBGM = function(what,repeat,speed) {
	what = Musickify(what);
	this.RepeatSave = repeat;
	var tvol1 = undefined;
	var tvol2 = undefined;
	if(!this.CrossfadeUpper) {
		this.BGM2 = LoadSound(what,this.stream);
		this.BGM2.play(repeat);
		tvol1 = this.BGM.getVolume();
		tvol2 = this.BGM2.getVolume();
		this.BGM2.setVolume(0);
		for(var i = this.BGM.getVolume(); i >= 0; i -= speed) {
			if(this.BGM2.getVolume() > this.DefaultVolume) break;
			this.BGM.setVolume(i);
			this.BGM2.setVolume(tvol1 - i);
			wait(1);
		}
		this.FadeToLevelBGM(0,speed);
		this.CrossfadeUpper = true;
		this.FadeToLevelBGM(this.DefaultVolume,speed);
		this.CrossfadeUpper = false;
	} else {
		this.BGM = LoadSound(what,this.stream);
		this.BGM.play(repeat);
		tvol1 = this.BGM.getVolume();
		tvol2 = this.BGM2.getVolume();
		this.BGM.setVolume(0);
		for(var i = this.BGM2.getVolume(); i >= 0; i -= speed) {
			if(this.BGM.getVolume() > this.DefaultVolume) break;
			this.BGM2.setVolume(i);
			this.BGM.setVolume(tvol2 - i);
			wait(1);
		}
		this.FadeToLevelBGM(0,speed);
		this.CrossfadeUpper = false;
		this.FadeToLevelBGM(this.DefaultVolume,speed);
		this.CrossfadeUpper = true;
	}
	this.CrossfadeUpper = !this.CrossfadeUpper;
	return this.CrossfadeUpper;
}
JukeboxService.prototype.PlayBGM = function(what,repeat) {
	this.CurrentlyPlaying = what;
	this.CrossfadeUpper = false;
	this.BGM.stop();
	this.BGM = what;
	this.BGM.setVolume(this.DefaultVolume);
	this.RepeatSave = repeat;
	this.BGM.setVolume(this.DefaultVolume);
	this.BGM.play(repeat);
}
JukeboxService.prototype.FadeInAndPlayBGM = function(what,repeat,speed) {
	this.CurrentlyPlaying = what;
	this.CrossfadeUpper = false;
	this.BGM.stop();
	this.BGM = what;
	this.RepeatSave = repeat;
	this.BGM.play(repeat);
	this.BGM.setVolume(0);
	this.FadeToLevelBGM(this.DefaultVolume,speed);
}
JukeboxService.prototype.CrossfadeBGM = function(what,repeat,speed) {
	this.RepeatSave = repeat;
	var tvol1 = undefined;
	var tvol2 = undefined;
	if(!this.CrossfadeUpper) {
		this.BGM2.stop();
		this.BGM2 = what;
		this.BGM2.play(repeat);
		tvol1 = this.BGM.getVolume();
		tvol2 = this.BGM2.getVolume();
		this.BGM2.setVolume(0);
		for(var i = this.BGM.getVolume(); i >= 0; i -= speed) {
			if(this.BGM2.getVolume() > this.DefaultVolume) break;
			this.BGM.setVolume(i);
			this.BGM2.setVolume(tvol1 - i);
			wait(1);
		}
		this.FadeToLevelBGM(0,speed);
		this.CrossfadeUpper = true;
		this.FadeToLevelBGM(this.DefaultVolume,speed);
		this.CrossfadeUpper = false;
	} else {
		this.BGM.stop();
		this.BGM = what;
		this.BGM.play(repeat);
		tvol1 = this.BGM.getVolume();
		tvol2 = this.BGM2.getVolume();
		this.BGM.setVolume(0);
		for(var i = this.BGM2.getVolume(); i >= 0; i -= speed) {
			if(this.BGM.getVolume() > this.DefaultVolume) break;
			this.BGM2.setVolume(i);
			this.BGM.setVolume(tvol2 - i);
			wait(1);
		}
		this.FadeToLevelBGM(0,speed);
		this.CrossfadeUpper = false;
		this.FadeToLevelBGM(this.DefaultVolume,speed);
		this.CrossfadeUpper = true;
	}
	this.CrossfadeUpper = !this.CrossfadeUpper;
	return this.CrossfadeUpper;
}
var Jukebox = new JukeboxService(255);
try {notafunction();} catch(k) {TotalLineCount += k.lineNumber+1; FileLineCount[k.fileName.replace(/scripts\//i,"")] = k.lineNumber+1;}