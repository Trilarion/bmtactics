// Betrayer's Moon Project Sound Effect Library

function SFXify(what) {
	return "sfx/" + what;
}

function LoadSFX(what) {
	return LoadSound(SFXify(what));
}

var SFXDB = new Array();

IncludeSFX("accept.wav"); // Public Domain
IncludeSFX("accept2.wav"); // Public Domain
IncludeSFX("coins.wav"); // GPL, from WorldForge.org

for(var i = 0; i < SoundFX.length; i++) {
	SFXDB[SoundFX[i].match(/(\w+)\w/)[0]] = LoadSFX(SoundFX[i]);
}

function PlaySoundEffect(what) {
	SFXDB[what].stop();
	SFXDB[what].play(false);
}
try {notafunction();} catch(k) {TotalLineCount += k.lineNumber+1; FileLineCount[k.fileName.replace(/scripts\//i,"")] = k.lineNumber+1;}