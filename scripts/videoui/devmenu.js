/* BMPROJECT FILE: base/devmenu.js - The Developer Menu

SYNOPSIS:
	This file defines the DeveloperMenu() function and functions that are
	used by it. The Developer Menu is a sort of debug menu that enables
	easier access to internal functions, primarily for alpha/beta testers.

EXECUTION:
	None - all execution of the Developer Menu is handled by game().

CONTENT:
	Menus that contain information about internal functions, option
	controls, BGM/SFX tests, information about new features in this
	version, etc.

LICENSE(S):
	GNU General Public License (see zeroinit.js)

*/
// The DeveloperMenu() and related functions (for Betrayer's Moon Project).
// By dremelofdeath 2005/2006 (GPL)
// Broke from zeroinit.js on 01/29/06 11:03 PM EST

// NoMenuHere() placeholder is for the DeveloperMenu... it'll placehold for the
// menus that have not yet been coded
function NoMenuHere(id) {
	ClearScreenBuffer();
	FlashBox(StartWindowStyle,"This menu (" + id + ") has not yet been implemented. It should be either implemented in a further release or removed. Sorry for the inconvenience.","SYSTEM NOTIFICATION","middle");
	FlipScreen();
	GetKey();
	return "main";
}

function InvalidPage(page) {
	this.name = "InvalidPage";
	this.message = page + " is not a valid page";
	this.description = this.message;
	this.fileName = "scripts/base/devmenu.js";
	this.lineNumber = -2;
}
InvalidPage.prototype = new Error;

// NOTE TO SELF: InputArgument and ChangeOption need merged/cleaned up

try {notafunction();} catch(v) {TestRecord("InputArgument",new Array("argument"),v.lineNumber+2,v.fileName,false);}
function InputArgument(argument) {return Input_BoxLibrary("INPUT ARGUMENT: ",argument);}

try {notafunction();} catch(v) {TestRecord("ChangeOption",new Array("option"),v.lineNumber+2,v.fileName,false);}
function ChangeOption(option) {return Input_BoxLibrary("CHANGING OPTION: ",option);}

// DeveloperMenu() function is the Developer Menu for testing purposes
function DeveloperMenu() {
	var devpref_file = OpenFile("devprefs.cfg");
	var DevPrefs = new Array();
	var DevPrefTypes = new Array();
	DevPrefs["enablemusic"] = devpref_file.read("enablemusic",true);
	DevPrefTypes["enablemusic"] = "bool";
	if(DevPrefs["enablemusic"]) Jukebox.LoadPlayBGM("devmenu.ogg",true,2);
	var DevMenuHecho = false; // Is the DevMenu done configuring?
	var DevMenuPantalla = false; // Screen buffer variable... i.e. GetScreenBuffer()
	var DevMenuTecla = false; // Variable that holds key that's pressed
	var DevMenuPagina = "main"; // Current page variable
	var DevMenuElige = 0; // the decisionid returned to game() by DeveloperMenu()
	var sfxdump,fstrspace,input,inputbuf,funcret,optdump,scrdump;
	var sfxindex = 0;
	var funcindex = 0;
	var functte = 0;
	var homogayhack = false; // don't touch this...
	var optindex = 0;
	var scrindex = 0;
	do { // *bites into Dark Chocolate Pocky* //
		switch(DevMenuPagina) {
			case "main":
				ClearScreenBuffer();
				FlashBox(StartWindowStyle,"VERSION: " + CurrentVersionString.toUpperCase() + " - " + VersionTitle +
					"\n   >ESCAPE: Quit " + GameTitle +
					"\n   >F1: Tester Instructions" +
					"\n   >F2: New Features 0.2.0 > 0.2.1" +
					"\n   >F3: SFX/BGM Test" +
					//"\n   >F3: Configure in-game scripts" +
					//"\n   >F4: View build options" +
					"\n   >F4: Function Test" +
					"\n   >F5: Line Count Statistics" +
					"\n   >TAB: Configure Developer Menu Options" +
					"\n   >ENTER: Begin normal game with current settings" +
					"\n   >SHIFT: Begin debug mode with current settings" +
					"\n   >CTRL: Begin game in test map with current settings"+
					"\n\n\n\n\n\n\n\n\n\n\n\nIncludes took " + IncludeTime + "ms to start.\nProgram took " + StartupTime + "ms to start."
					,GameTitle.toUpperCase() + " DEVELOPER MENU","fullscreen-dev");
				DevMenuPantalla = GetScreenBuffer();
				FlipScreen(); // BUGFIX 0.2.2: Screen does not flip when key is held
				while(!(AreKeysLeft() && IsAnyKeyPressed())) {
					DevMenuPantalla.blit(0,0);
					FlipScreen();
				}
				DevMenuPantalla.blit(0,0);
				DevMenuTecla = GetKey();
				switch(DevMenuTecla) {
					case KEY_ESCAPE:
						GameExit();
						break;
					case KEY_F1:
						DevMenuPagina = "instructions";
						break;
					case KEY_F2:
						DevMenuPagina = "newfeatures";
						break;
					case KEY_F3:
						DevMenuPagina = "sfxbgm";
						break;
					case KEY_F4:
						DevMenuPagina = "functest";
						break;
					case KEY_F5:
						DevMenuPagina = "stats";
						break;
					case KEY_F6:
						DevMenuPagina = "build-options";
						break;
					case KEY_TAB:
						DevMenuPagina = "options";
						break;
					case KEY_ENTER:
						PlaySoundEffect("accept");
						Transition.DefaultMelt(GetScreenBuffer());
						DevMenuElige = 0;
						DevMenuHecho = true;
						break;
				}
				break;
			case "instructions":
				ClearScreenBuffer();
				FlashBox(StartWindowStyle,"If you are a game tester, begin to test the game by" +
				"\nrunning the default settings. Finding a bug could be" +
				"\nanything from a complete game crash to a minor" +
				"\ngraphics glitch.\n\n" +
				"If you do find a bug, before you submit a report, try changing relevant settings. If you find a setting that circumvents the bug or error, include that setting with your report.\n\n" +
				"In either case, however, send your detailed report to dremelofdeath@users.sourceforge.net or by submitting a bug report on the BM SourceForge project page at http://www.sf.net/projects/bm-game.\n" +
				"\n   >ESCAPE: Quit " + GameTitle +
				"\n   >BACKSPACE: Return to main menu",GameTitle.toUpperCase() + " DEVELOPER MENU - INSTRUCTIONS","fullscreen-dev");
				DevMenuPantalla = GetScreenBuffer();
				FlipScreen(); // BUGFIX 0.2.2: Screen does not flip when key is held
				while(!(AreKeysLeft() && IsAnyKeyPressed())) {
					DevMenuPantalla.blit(0,0);
					FlipScreen();
				}
				DevMenuPantalla.blit(0,0);
				DevMenuTecla = GetKey();
				switch(DevMenuTecla) {
					case KEY_ESCAPE:
						GameExit();
						break;
					case KEY_BACKSPACE:
						DevMenuPagina = "main";
						break;
				}
				break;
			case "build-options":
				ClearScreenBuffer();
				FlashBox(StartWindowStyle,
				"GameTitle = " + GameTitle +
				"\nCurrentVersion = " + CurrentVersion/*[0] + "," + CurrentVersion[1] + "," + CurrentVersion[2]*/ +
				"\nVersionTitle = " + VersionTitle +
				"\nShowDeveloperMenu = " + ShowDeveloperMenu +
				"\nStepThroughLoad = " + StepThroughLoad +
				"\nForcePreloaderSlow = " + ForcePreloaderSlow +
				"\nPreloaderSlowdown = " + PreloaderSlowdown +
				"\nShowPreloader = " + ShowPreloader +
				"\n\n   >ESCAPE: Quit " + GameTitle +
				"\n   >BACKSPACE: Return to main menu",GameTitle.toUpperCase() + " DEVELOPER MENU - BUILD OPTIONS","fullscreen-dev");
				DevMenuPantalla = GetScreenBuffer();
				FlipScreen(); // BUGFIX 0.2.2: Screen does not flip when key is held
				while(!(AreKeysLeft() && IsAnyKeyPressed())) {
					DevMenuPantalla.blit(0,0);
					FlipScreen();
				}
				DevMenuPantalla.blit(0,0);
				DevMenuTecla = GetKey();
				switch(DevMenuTecla) {
					case KEY_ESCAPE:
						GameExit();
						break;
					case KEY_BACKSPACE:
						DevMenuPagina = "main";
						break;
				}
				break;
			case "sfxbgm":
				if(DevPrefs["enablemusic"]) Jukebox.PauseBGM();
				ClearScreenBuffer();
				sfxdump = GetArrayElementNames(SFXDB);
				FlashBox(StartWindowStyle,
				"Use the arrow keys to control which SFX/BGM you want to test." +
				"\n\nCurrent SFX: " + sfxdump[sfxindex] +
				"\nSFX Index: " + (sfxindex+1) + "/" + sfxdump.length +
				"\n\nCurrent BGM: " + // insert BGM stuffs here
				"\nBGM Index: " + // and here...
				"\n\n   >ENTER:  Play current SFX" + 
				"\n   >SPACE: Play current BGM" +
				"\n   >LEFT/RIGHT: Change SFX" + 
				"\n   >UP/DOWN: Change BGM" +
				"\n   >ESCAPE: Quit " + GameTitle +
				"\n   >BACKSPACE: Return to main menu",GameTitle.toUpperCase() + " DEVELOPER MENU - SFX/BGM TEST","fullscreen-dev");
				DevMenuPantalla = GetScreenBuffer();
				FlipScreen(); // BUGFIX 0.2.2: Screen does not flip when key is held
				while(!(AreKeysLeft() && IsAnyKeyPressed())) {
					DevMenuPantalla.blit(0,0);
					FlipScreen();
				}
				DevMenuPantalla.blit(0,0);
				DevMenuTecla = GetKey();
				switch(DevMenuTecla) {
					case KEY_ENTER:
						PlaySoundEffect(sfxdump[sfxindex]);
						break;
					case KEY_LEFT:
						if(sfxindex > 0) sfxindex--;
						break;
					case KEY_RIGHT:
						if(sfxindex < sfxdump.length - 1) sfxindex++;
						break;
					case KEY_ESCAPE:
						GameExit();
						break;
					case KEY_BACKSPACE:
						if(DevPrefs["enablemusic"]) Jukebox.ResumeBGM();
						DevMenuPagina = "main";
						break;
				}
				break;
			case "newfeatures":
				FlashBox(StartWindowStyle,
				"The new features and changes from 0.2.0 > 0.2.1:" +
				"\n\nEven more code cleanup and optimization." +
				"\n\nRewrite of keyboard input driver... much more stable/effective now." +
				"\n\nLine counting algorithms were installed... check it out in the devmenu." +
				"\n\n   >ESCAPE: Quit " + GameTitle +
				"\n   >BACKSPACE: Return to main menu",GameTitle.toUpperCase() + " DEVELOPER MENU - NEW FEATURES","fullscreen-dev");
				DevMenuPantalla = GetScreenBuffer();
				FlipScreen(); // BUGFIX 0.2.2: Screen does not flip when key is held
				while(!(AreKeysLeft() && IsAnyKeyPressed())) {
					DevMenuPantalla.blit(0,0);
					FlipScreen();
				}
				DevMenuPantalla.blit(0,0);
				DevMenuTecla = GetKey();
				switch(DevMenuTecla) {
					case KEY_ESCAPE:
						GameExit();
						break;
					case KEY_BACKSPACE:
						DevMenuPagina = "main";
						break;
				}
				break;
			case "functest":
				fstrspace = "";
				homogayhack = false;
				for(var i = 0; i < FTDB[funcindex].args.length; i++) {
					fstrspace += FTDB[funcindex].args[i];
					fstrspace = (i==FTDB[funcindex].args.length-1)? fstrspace:fstrspace+", ";
				}
				ClearScreenBuffer();
				FlashBox(StartWindowStyle,
				"Use the arrow keys to control which function you want to test. After you make your selection, the function will be tested as if it were being executed in-game." +
				"\n\nWARNING! Exiting after a crash is disabled here. The game will NOT exit if the function you invoke crashes. It it possible that this could corrupt data, so be careful." +
				"\n\nTesting ID#: " + (funcindex+1) + "/" + FTDB.length +
				"\nFunction: " + FTDB[funcindex].name + "()" +
				"\n# of arguments: " + FTDB[funcindex].args.length +
				"\nArguments: " + fstrspace +
				"\nDeclared at: line " + FTDB[funcindex].linenumber + " in " + FTDB[funcindex].filename +
				"\n\n   >ENTER: Execute current function" + 
				"\n   >LEFT/RIGHT: Change function" + 
				"\n   >ESCAPE: Quit " + GameTitle +
				"\n   >BACKSPACE: Return to main menu",GameTitle.toUpperCase() + " DEVELOPER MENU - FUNCTION TEST","fullscreen-dev");
				DevMenuPantalla = GetScreenBuffer();
				FlipScreen(); // BUGFIX 0.2.2: Screen does not flip when key is held
				while(!(AreKeysLeft() && IsAnyKeyPressed())) {
					DevMenuPantalla.blit(0,0);
					FlipScreen();
				}
				DevMenuPantalla.blit(0,0);
				DevMenuTecla = GetKey();
				switch(DevMenuTecla) {
					case KEY_ENTER:
						input = new Array();
						for(var i = 0; i < FTDB[funcindex].args.length; i++) {
							inputbuf = InputArgument(FTDB[funcindex].args[i]);
							if(inputbuf == KEY_ESCAPE) {
								break;
							} else if(inputbuf == "") {
								input.push("undefined");
							} else if(!(inputbuf == "")) {
								input.push(inputbuf);
							}
						}
						if(inputbuf == KEY_ESCAPE) break;
						try {
							ClearKeyQueue();
							StartDebugTimer();
							funcret = EmulateFunctionExecution(FTDB[funcindex].name,input);
							functte = GetDebugTimeElapsed();
						} catch(testerror) { //// OH MY GOD, THIS CODE IS TICKING ME OFF
							homogayhack = HandleTestException(testerror)
						}	finally { ///!!!! GRAH!!
							StopDebugTimer();
						}
						if(FTDB[funcindex].needsflipped || (homogayhack && !FTDB[funcindex].needsflipped)) {
							WaitForKeyPress();
							ClearKeyQueue();
							// IF ANYBODY EVER READS THIS, IT TOOK ME TWO FREAKING HOURS TO GET THE LAST
							// TEN LINES OF CODE RIGHT. YOU HAD BEST APPRECIATE MY HARD WORK. -Zachary Murray
						} // Look on the bright side: it works now...
						ClearScreenBuffer();
						FlashBox(StartWindowStyle,
						FTDB[funcindex].name + "(" + input + ");" +
						"\n\nValue returned: " + funcret +
						"\nTime to execute: " + functte + "ms",
						"TEST RESULTS","middle");
						WaitForKeyPress();
						ClearKeyQueue();
						break;
					case KEY_LEFT:
						if(funcindex > 0) funcindex--;
						break;
					case KEY_RIGHT:
						if(funcindex < FTDB.length - 1) funcindex++;
						break;
					case KEY_ESCAPE:
						GameExit();
						break;
					case KEY_BACKSPACE:
						DevMenuPagina = "main";
						break;
				}
				break;
			case "options":
				ClearScreenBuffer();
				optdump = GetArrayElementNames(DevPrefs);
				FlashBox(StartWindowStyle,
				"Use the arrow keys to control which option you want to change. devprefs.js will be reloaded on the next restart. Please make sure to restart after saving changes." +
				"\n\nOption Index: " + (optindex+1) + "/" + optdump.length +
				"\nOption: " + optdump[optindex] +
				"\nValue: " + DevPrefs[optdump[optindex]] +
				"\n\n   >ENTER:  Save changes to devprefs.cfg" + 
				"\n   >SPACE: Toggle current option/edit value" +
				"\n   >LEFT/RIGHT: Change option" + 
				"\n   >END: Soft restart" +
				"\n   >ESCAPE: Quit " + GameTitle +
				"\n   >BACKSPACE: Return to main menu",GameTitle.toUpperCase() + " DEVELOPER MENU - OPTIONS","fullscreen-dev");
				DevMenuPantalla = GetScreenBuffer();
				FlipScreen(); // BUGFIX 0.2.2: Screen does not flip when key is held
				while(!(AreKeysLeft() && IsAnyKeyPressed())) {
					DevMenuPantalla.blit(0,0);
					FlipScreen();
				}
				DevMenuPantalla.blit(0,0);
				DevMenuTecla = GetKey();
				switch(DevMenuTecla) {
					case KEY_ENTER:
						ClearScreenBuffer();
						FlashBox(StartWindowStyle,"Please wait... saving devprefs.js.","SAVING: DEVPREFS.JS","middle");
						FlipScreen();
						for(var i = 0; i < optdump.length; i++) {
							devpref_file.write(optdump[i],DevPrefs[optdump[i]]);
						}
						devpref_file.flush();
						ClearScreenBuffer();
						ClearKeyQueue();
						FlashBox(StartWindowStyle,"Save successful.","DEVPREFS.JS SAVED","middle");
						WaitForKeyPress();
						ClearKeyQueue();
						break;
					case KEY_SPACE:
						if(DevPrefTypes[optdump[optindex]] == "bool") {
							DevPrefs[optdump[optindex]] = !DevPrefs[optdump[optindex]];
						} else {
							DevPrefs[optdump[optindex]] = ChangeOption(optdump[optindex]);
						}
						break;
					case KEY_LEFT:
						if(optindex > 0) optindex--;
						break;
					case KEY_RIGHT:
						if(optindex < optdump.length - 1) optindex++;
						break;
					case KEY_END:
						FlushScreenBuffer();
						wait(2);
						Jukebox.StopBGM();
						ForceSoftRestart = true;
						return "x";
						break;
					case KEY_ESCAPE:
						GameExit();
						break;
					case KEY_BACKSPACE:
						DevMenuPagina = "main";
						break;
				}
				break;
			case "stats":
				ClearScreenBuffer();
				scrdump = GetArrayElementNames(FileLineCount);
				FlashBox(StartWindowStyle,
				"Use the arrow keys to select which file's statistics you would like to review. Please note that these statistics reflect only original lines of code (not libraries... the lib directory)." +
				"\n\nTotal lines of code: " + TotalLineCount +
				"\nCurrent file: " + scrdump[scrindex] + " (" + (scrindex+1) + "/" + scrdump.length + ")" +
				"\nLines of code in file: " + FileLineCount[scrdump[scrindex]] +
				"\nFile's percentage of code in program: " + (roundto(FileLineCount[scrdump[scrindex]]/TotalLineCount*100,2)) + "%" +
				"\nAverage lines of code per file: " + (roundto(TotalLineCount/scrdump.length,2)) +
				"\n\n   >LEFT/RIGHT: Change file" +
				"\n   >ESCAPE: Quit " + GameTitle +
				"\n   >BACKSPACE: Return to main menu",GameTitle.toUpperCase() + " DEVELOPER MENU - STATISTICS","fullscreen-dev");
				DevMenuPantalla = GetScreenBuffer();
				FlipScreen(); // BUGFIX 0.2.2: Screen does not flip when key is held
				while(!(AreKeysLeft() && IsAnyKeyPressed())) {
					DevMenuPantalla.blit(0,0);
					FlipScreen();
				}
				DevMenuPantalla.blit(0,0);
				DevMenuTecla = GetKey();
				switch(DevMenuTecla) {
					case KEY_LEFT:
						if(scrindex > 0) scrindex--;
						break;
					case KEY_RIGHT:
						if(scrindex < scrdump.length - 1) scrindex++;
						break;
					case KEY_ESCAPE:
						GameExit();
						break;
					case KEY_BACKSPACE:
						DevMenuPagina = "main";
						break;
				}
				break;
			case "scripts":
				DevMenuPagina = NoMenuHere("scripts");
				break;
			default:
				ClearScreenBuffer();
				throw new InvalidPage(DevMenuPagina);
				break;
		}
	} while(!DevMenuHecho); // Is it obvious I'm bilingual? --Zach
	Jukebox.FadeOutBGM(2);
	ClearScreenBuffer();
	return DevMenuElige;
} // DeveloperMenu()
try {notafunction();} catch(k) {TotalLineCount += k.lineNumber+1; FileLineCount[k.fileName.replace(/scripts\//i,"")] = k.lineNumber+1;}