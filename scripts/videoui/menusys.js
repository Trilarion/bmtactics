/* BMPROJECT FILE: menu/menus.js - Menu systems and the Menu object

SYNOPSIS:
	This script allows for the drawing of menus and option selection
	windows to be drawn and activated.

EXECUTION:
	No execution - this script only defines how other menus are defined,
	not the menus themselves.

CONTENT:
	Contains the menu systems, drawing functions, the menu object, and 
	others.

NOTES:
	This is the Betrayer's Moon Project menu system. Obviously, changes
	made affect everything that has uses a menu. This is where much of
	the UI will be defined.

LICENSE(S):
	GNU General Public License (see zeroinit.js)

*/

// defaults for all menus
const MenuCursor = LoadImage("ui/cursor.png");
const MenuDownArrow = WindowMoreImage; // don't load two copies, that's a waste of memory.
const MenuUpArrow = LoadImage("ui/curup.png");
const MenuCursorSpaceRatio = 1.25;
const MenuCursorSpace = MenuCursorSpaceRatio*MenuCursor.width;

function KeyAlreadyUsedError(key) {
  this.name = "KeyAlreadyUsedError";
  this.message = ConvertKeyToString(key)+" is already used by this menu";
  this.description = this.message;
}
KeyAlreadyUsedError.prototype = new Error;

function MenuItem(text,action,key) {
	this.nested = false;
	this.text = text;
	this.key = key? key : false; // key here is a shortcut key
	this.action = action; // this should be a function
}

function ItemNextMenu(text,menu_object,xo,yo,wo,ho,title_override,style_override,key) {
	this.nested = true;
	this.text = text;
	this.key = key? key : false;
	this.menu = menu_object;
	this.xo = xo;
	this.yo = yo;
	this.wo = wo;
	this.ho = ho;
	this.style_override = style_override;
	this.title_override = title_override;
	this.action = function(__internal_cmo) {
		this.menu.nestedcall = true;
		return this.menu.nestgo(__internal_cmo,this.xo,this.yo,this.wo,this.ho,this.title_override,this.style_override);
	}
}

function Menu(x,y,w,h) {
	if(!(this instanceof Menu)) return new Menu(x,y,w,h);
	
	// We will need EVERY aspect of a menu defined here.
	
	this.x = x? x:1; this.y = y? y:1; this.w = w? w:1; this.h = h? h:1; // main coordinates
	this.width = this.w; this._width = this.w; this.height = this.h; this._height = this.h; // symlinks
	this.bx = x? x:1; this.by = y? y:1; this.bw = w? w:1; this.bh = h? h:1; // backups
	
	this.fx = 1; // these are for residual nesting... this method is used instead of passing fx and fy
	this.fy = 1;
	this.ss = false;
	this.__start_args = false;
	
	this.isparent = false; // this is only true when it is called by the go method
	this.nest_calling_copy = false;
	
	this.strictsize = false;
	
	this.title = "";
	this.style = userprefs["style"].current;
	
	this.items = new Array();
	this.defaultposition = 0;
	
	this.cursor = MenuCursor;
	this.uparrow = MenuUpArrow;
	this.downarrow = MenuDownArrow;
	this.cursorspace = MenuCursorSpace;
	this.ratio = MenuCursorSpaceRatio;
	
	this.cursorpos = 1; // this one's for a residual cursor while nesting
	
	this.bgupdate = true; // setting this to true lets GetScreenBuffer() render before the menu
	this.renderscript = undefined; // define this (manually) only if something needs to be updated/rendered before the menu is... it's typeof function.
	
	this.offset = Math.floor(TextFontHeight/2);
	
	this.canbekilled = true;
	this.okillkey = false;
	this.oselectkey = false;
	this.oupkey = false; // these are only overrides set by their respective methods
	this.odownkey = false;
	
	this.scrolling = false;
	this.itemh = 0; // for scrolling menus
	this.numberofitems = 0; // also for scrolling menus
	
	this.shading = false; // enabling this makes the menu pull down when starting and exiting //*
	
	this.lastdecision = undefined;
	
	this.denynextcalltoggle = false; // this is set by this.DenyNextCall() and autokills the next call to the menu... reset after that call
	this.denynextcallonce = false;
	
	// The style is handled (or will be handled) by the user's preferences.
}

Menu.prototype.addItem = function(text,action,key) {
	if(!this.verifyKeyIsUnused(key)) throw new KeyAlreadyUsedError(key);
	return this.items.push(new MenuItem(text,action,key));
}

Menu.prototype.addMenuAsItem = function(text,menu_object,xo,yo,wo,ho,title_override,style_override,key) { // should work with other methods...
	if(!this.verifyKeyIsUnused(key)) throw new KeyAlreadyUsedError(key); // this technically adds a MenuItem compliant item
	return this.items.push(new ItemNextMenu(text,menu_object,xo,yo,wo,ho,title_override,style_override,key));
}

Menu.prototype.delItem = function(which) {
	if(which == 0) {
		return this.items.shift();
	}
	var upsilon = new Array();
	while(this.items.length-1 >= which) {
		upsilon.unshift(this.items.pop());
	}
	var omicron = upsilon.shift();
	var l = upsilon.length+0;
	for(var i = 0; i < l; i++) {
		this.items.push(upsilon.shift());
	}
	return omicron;
}

// insertItem inserts the option before where
Menu.prototype.insertItem = function(text,action,key,where) {
	if(!this.verifyKeyIsUnused(key)) throw new KeyAlreadyUsedError(key);
	if(where >= this.items.length) {
		return this.items.push(new MenuItem(text,action,key));
	}
	var upsilon = new Array();
	while(this.items.length > where) {
		upsilon.unshift(this.items.pop());
	}
	this.items.push(new MenuItem(text,action,key));
	var l = upsilon.length+0;
	for(var i = 0; i < l; i++) {
		this.items.push(upsilon.shift());
	}
	return this.items;
}

Menu.prototype.setStrictSize = function(onoroff) {
	this.strictsize = onoroff? true : false;
	return this.strictsize;
}

Menu.prototype.setTitle = function(titleit) {
	this.title = titleit.toUpperCase();
	return this.title;
}

Menu.prototype.setStyle = function(styleit) {
	this.style = styleit;
	return this.style;
}

Menu.prototype.sizeRevision = function() {
	this.x = this.bx+0; this.y = this.by+0; this.w = this.bw+0; this.h = this.bh+0; // restore backups
	var hSpaceRequired = TextFontHeight*this.items.length+2*this.offset; // 12:34 AM!!! (03-11-06)
	var w_currentbest = TextFont.getStringWidth(this.items[0].text);
	for(var i = 1; i < this.items.length; i++) {
		if(TextFont.getStringWidth(this.items[i].text) > w_currentbest) w_currentbest = TextFont.getStringWidth(this.items[i].text);
	}
	var wSpaceRequired = w_currentbest+this.cursorspace+2*this.offset;
	wSpaceRequired += !this.scrolling? 0 : this.uparrow.width > this.downarrow.width? 1.2*this.uparrow.width : 1.2*this.downarrow.width;
	if(this.h < hSpaceRequired) this.h = hSpaceRequired;
	if(this.w < wSpaceRequired) this.w = wSpaceRequired;
}

Menu.prototype.setDefaultPosition = function(position) {
	if(typeof position == "number") this.defaultposition = position;
	return this.defaultposition;
}

Menu.prototype.setCursor = function(imageorfilename) {
	this.cursor = typeof imageorfilename == "string" ? LoadImage(imageorfilename) : imageorfilename;
	this.cursorspace = this.ratio*this.cursor.width;
	return this.cursor;
}

Menu.prototype.setUpArrow = function(arrowimage) {
	this.uparrow = arrowimage;
	return this.uparrow;
}

Menu.prototype.setDownArrow = function(arrowimage) {
	this.downarrow = arrowimage;
	return this.downarrow;
}

Menu.prototype.setSizeRatio = function(ratio) {
	this.ratio = ratio > 1 ? ratio : 1;
	this.cursorspace = this.ratio*this.cursor.width;
	return this.ratio;
}

Menu.prototype.setBackgroundUpdate = function(trueorfalse) {
	this.bgupdate = trueorfalse;
}

Menu.prototype.setKillable = function(iskillable) {
	this.canbekilled = iskillable;
	return this.canbekilled;
}

Menu.prototype.setKillKey = function(key) {
	this.okillkey = key;
	return this.okillkey;
}

Menu.prototype.setSelectKey = function(key) {
	this.oselectkey = key;
	return this.oselectkey;
}

Menu.prototype.setUpKey = function(key) {
	this.oupkey = key;
	return this.oupkey;
}

Menu.prototype.setDownKey = function(key) {
	this.odownkey = key;
	return this.odownkey;
}

Menu.prototype.unsetKillKey = function() {
	this.okillkey = false;
	return this.okillkey;
}

Menu.prototype.unsetSelectKey = function() {
	this.oselectkey = false;
	return this.oselectkey;
}

Menu.prototype.unsetUpKey = function() {
	this.oupkey = false;
	return this.oupkey;
}

Menu.prototype.unsetDownKey = function() {
	this.odownkey = false;
	return this.odownkey;
}

Menu.prototype.setScrolling = function(onoroff,numberofitems) { // number of items is optional if onoroff is false
	if(onoroff) {
		this.scrolling = true;
		this.itemh = numberofitems*TextFontHeight+2*this.offset;
	} else { 
		this.scrolling = false;
	}
	this.numberofitems = numberofitems;
	return this.scrolling;
}

Menu.prototype.setBindScrolling = function(onoroff) { // this allows you to set the highest numberofitems within your size restrictions
	var numberofitems = Math.floor(this.h/TextFontHeight);
	if(onoroff) {
		this.scrolling = true;
		this.itemh = numberofitems*TextFontHeight+2*this.offset;
	} else { 
		this.scrolling = false;
	}
	this.numberofitems = numberofitems;
	return this.scrolling;
}

Menu.prototype.reset = function() {
	// BE CAREFUL WHEN USING RESET! You will lose all changes and the menu will revert to how it was when declared.
	this.x = this.bx+0; this.y = this.by+0; this.w = this.bw+0; this.h = this.bh+0; // restore backups
	this.title = "";
	this.style = userprefs["style"].current;
	this.items = new Array();
	this.cursor = MenuCursor;
	this.cursorspace = MenuCursorSpace;
	this.ratio = MenuCursorSpaceRatio;
	this.offset = Math.floor(TextFontHeight/2);
	this.canbekilled = true;
	this.okillkey = false;
	this.oselectkey = false;
	this.oupkey = false;
	this.odownkey = false;
}

Menu.prototype.blitCursor = function(x,y,w,h) {
	if(w == undefined || h == undefined) {
		this.cursor.blit(x,y);
	} else {
		this.cursor.transformBlit(x,y,x+w,y,x+w,y+h,x,y+h);
	}
}

Menu.prototype.displayCursor = function(position,menuxo,menuyo) {
	var fx = menuxo? menuxo : this.x;
	var fy = menuyo? menuyo : this.y;
	var delta = (TextFontHeight - this.cursor.height)/2-2;
	this.blitCursor(fx+this.offset,fy+this.offset+position*TextFontHeight+delta);
}

Menu.prototype.getKeysUsed = function() {
	var KeysUsed = new Array();
	KeysUsed.push(this.killkey);
	KeysUsed.push(this.selectkey);
	KeysUsed.push(this.upkey);
	KeysUsed.push(this.downkey);
	for(var i = 0; i < this.items.length; i++) {
		if(this.items[i].key) KeysUsed.push(this.items[i].key);
	}
	return KeysUsed;
}

Menu.prototype.getItemKeys = function() {
	var KeysUsed = new Array();
	for(var i = 0; i < this.items.length; i++) {
		if(this.items[i].key) KeysUsed.push(this.items[i].key);
	}
	return KeysUsed;
}

Menu.prototype.getMirrorItemKeys = function() {
	var KeysUsed = new Array();
	for(var i = 0; i < this.items.length; i++) {
		KeysUsed.push(this.items[i].key);
	}
	return KeysUsed;
}

Menu.prototype.verifyKeyIsUnused = function(keytocheck) {
	var KeysUsed = this.getKeysUsed;
	for(var i = 0; i < KeysUsed.length; i++) {
		if(KeysUsed[i] == keytocheck) return false;
	}
	return true;
}

Menu.prototype.setDenyNextCallOnce = function() {
	this.denynextcallonce = true;
}

Menu.prototype.toggleDenyNextCall = function() {
	this.denynextcalltoggle = !this.denynextcalltoggle;
}

Menu.prototype.blit = function(xo,yo,wo,ho,title_override,style_override) {
	if(!this.strictsize) this.sizeRevision(); // the overrides are not taken into account because they are, after all, overrides
	
	var fx = xo? xo : this.x;
	var fy = yo? yo : this.y;
	var fw = wo? wo : this.w;
	var fh = ho? ho : this.h;
	var xtitle = title_override? title_override : this.title;
	var xstyle = style_override? style_override : this.style;
	
	UniversalBox(fx,fy,fw,fh,xstyle,false,xtitle);
	
	// TO FINISH: correct the size revision to allow for cursor space / blit the options
	// ideas: alpha blit for in animation, fading between selections, alpha-based menus, smooth cursor movement
	// maybe alpha blit/pull down?
	// maybe make fadedown a separate function, perhaps in effect.js?
	// maybe use SetClippingRectangle in UniversalBox? no, but I will need to in the itemblit, no I won't; well maybe... scrolling menus?
	// use SetClippingRectangle and add strict option to disable sizeRevision
	
	for(var i = 0; i < this.items.length; i++) {
		TextFont.drawText(fx+this.offset+this.cursorspace,fy+this.offset+i*TextFontHeight,this.items[i].text);
	}
} // end of the blit method... does this need removed? --dremelofdeath

function __start_args_object(xo,yo,wo,ho,title_override,style_override,position_override) {
	this.xo = xo;
	this.yo = yo;
	this.wo = wo;
	this.ho = ho;
	this.title_override = title_override;
	this.style_override = style_override;
	this.position_override = position_override;
} // a __start_args_object holds all of the arguments passed to a Menu.start() call

try {_notafunction();} catch(error) {postnote("Menu.start() needs to take messages into account... a new item object, maybe?",error);}

Menu.prototype.start = function(xo,yo,wo,ho,title_override,style_override,position_override) {
	this.__start_args = new __start_args_object(xo,yo,wo,ho,title_override,style_override,position_override);
	if(this.denynextcallonce) { // start is technically the blitter
		this.denynextcallonce = false;
		return undefined;
	}
	if(this.denynextcalltoggle) {return undefined;}
	
	if(!this.strictsize) this.sizeRevision(); // the overrides are not taken into account because they are, after all, overrides
	
	var fx = xo? xo : this.x;
	var fy = yo? yo : this.y;
	var fw = wo? wo : this.w;
	var fh = ho? ho : this.scrolling? this.itemh : this.h;
	var xtitle = title_override? title_override : this.title;
	var xstyle = style_override? style_override : this.style;
	
	this.fx = fx; // save these for nesting purposes
	this.fy = fy; // make sure you know the difference beween this.fx and fx... <this.> is for the nest save
	
	this.position = position_override? position_override : this.defaultposition; // smell the residualness
	var scrollcursorpos = 0;
	var scrollstart = this.position+0;
	
	var itemkeys = this.getMirrorItemKeys();
	
	var snapshot = GetScreenBuffer();
	
	this.ss = snapshot; // also for nesting
	// this.ss is different from ss... this is very important
	
	var done = false;
	
	var ss,retopt,keygot,scrollend;
	
	ClearScreenBuffer();
	
	while(!done) {
		ClearKeyQueue();
		if(this.bgupdate) snapshot.blit(0,0);
		if(typeof this.renderscript == "function") this.renderscript();
		UniversalBox(fx,fy,fw,fh,xstyle,false,xtitle); // scrollstart
		scrollend = this.scrolling? scrollstart + this.numberofitems : this.items.length;
		for(var i = scrollstart+0; i < scrollend; i++) {
			TextFont.drawText(fx+this.offset+this.cursorspace,fy+this.offset+(i-scrollstart)*TextFontHeight,this.items[i].text);
		} // scrollstart and scrollend take into account if it's scrolling or not...
		if(!this.scrolling) {
			this.displayCursor(this.position,fx,fy);
		} else {
			this.displayCursor(scrollcursorpos,fx,fy);
			if(scrollstart > 0) this.uparrow.blit(fx+fw-1.2*this.uparrow.width,fy+2);
			if(scrollend < this.items.length) this.downarrow.blit(fx+fw-1.2*this.downarrow.width,fy+fh-this.downarrow.height-2);
		}
		ss = GetScreenBuffer();
		FlipScreen();
		while(!(AreKeysLeft() && IsAnyKeyPressed())) {
			ss.blit(0,0);
			FlipScreen();
		}
		ss.blit(0,0);
		keygot = GetKey();
		switch(keygot) {
			case (this.okillkey ? this.okillkey : userprefs["cancelkey"].current):
				if(this.canbekilled) {
					done = true;
					retopt = -1; // -1 means the menu was killed... it's impossible to achieve otherwise
				}
				break;
			case (this.oselectkey ? this.oselectkey : userprefs["actionkey"].current):
				done = true;
				retopt = this.position;
				break;
			case (this.odownkey ? this.odownkey : userprefs["downkey"].current):
				done = false;
				if(this.scrolling && this.position == this.items.length-1) {
					this.position = 0;
					scrollcursorpos = 0;
					scrollstart = 0;
				} else {
					if(this.position < this.items.length-1) {
						this.position++;
						if(this.scrolling && !(scrollcursorpos < this.numberofitems-1)) scrollstart++;
					} else {
						if(!this.scrolling) this.position = 0;
					}
					if(this.scrolling && scrollcursorpos < this.numberofitems-1) scrollcursorpos++;
				}
				break;
			case (this.oupkey ? this.oupkey : userprefs["upkey"].current):
				done = false;
				if(this.scrolling && this.position == 0) {
					this.position = this.items.length-1;
					scrollcursorpos = this.numberofitems-1;
					scrollstart = this.items.length-this.numberofitems;
				} else {
					if(this.position > 0) {
						this.position--;
						if(this.scrolling && !(scrollcursorpos > 0)) scrollstart--;
					} else {
						if(!this.scrolling) this.position = this.items.length-1;
					}
					if(this.scrolling && scrollcursorpos > 0) scrollcursorpos--;
				}
				break;
			default:
				for(var i = 0; i < itemkeys.length; i++) {
					if(keygot == itemkeys[i]) {
						done = true;
						this.position = i;
						retopt = this.position;
					}
				} // default clause checks for any other keys added to the Menu
				break;
		}
	}
	this.lastdecision = retopt;
	GarbageCollect(); // attempt to free memory every time the menu terminates
	return retopt;
}

Menu.prototype.executeLastDecision = function() {
	if(this.lastdecision !== -1) this.items[this.lastdecision].action(this);
}

Menu.prototype.executeDecision = function(which) {
	if(!(which == -1 || which === undefined)) this.items[which].action(this);
	return which; // note the pass of this... it is ignored unless the action just so happens to be a nested menu
}

Menu.prototype.go = function(xo,yo,wo,ho,title_override,style_override) {
	this.isparent = true;
	return this.executeDecision(this.start(xo,yo,wo,ho,title_override,style_override));
}

try {_notafunction();} catch(error) {postnote("Menu.nestgo() needs to retain past cursor positions.",error);}

/* the nestgo method
 * !! WARNING !! THIS METHOD IS _DANGEROUS_ TO USE FREQUENTLY.
 * Granted, it serves its purposes, but it is horrible with memory managment.
 * If you're not careful, the stack will overflow and will crash.
 *
 * You have been warned!
 *
 * This function allows a menu to call another menu in a tree-like fashion.
 * Please note never to call nestgo by itself; nestgo is automatically called
 * when using the addMenuAsItem method.
 */

// I don't believe that you should ever need to call nestgo on its own. So don't.

Menu.prototype.nestgo = function(calling_menu_object,xo,yo,wo,ho,style_override,title_override) { // this is for menus inside of menus only... use the go method for others
	this.isparent = false;
	this.nest_calling_copy = calling_menu_object;
	var sao = calling_menu_object.__start_args; // shortness
	var fxo = (xo !== undefined)? xo : calling_menu_object.fx + CaptFontHeight;
	var fyo = (yo !== undefined)? yo : calling_menu_object.fy + CaptFontHeight;
	var ret = this.start(fxo,fyo,wo,ho,style_override,title_override);
	if(ret === -1 || ret === undefined) { // if canceled or denied, go back a menu
		ClearScreenBuffer();
		calling_menu_object.ss.blit(0,0); // this helps clean screen buffer garbage
		if(!calling_menu_object.isparent) {
			calling_menu_object.nestgo(calling_menu_object.nest_calling_copy,sao.xo,sao.yo,sao.wo,sao.ho,sao.title_override,sao.style_override,calling_menu_object.position);
		} else {
			calling_menu_object.go(sao.xo,sao.yo,sao.wo,sao.ho,sao.title_override,sao.style_override,calling_menu_object.position);
		}
	} else { // otherwise, an option was passed. execute it
		this.executeDecision(ret);
	}
	GarbageCollect(); // attempt to free some memory
}

try {notafunction();} catch(k) {TotalLineCount += k.lineNumber+1; FileLineCount[k.fileName.replace(/scripts\//i,"")] = k.lineNumber+1;}