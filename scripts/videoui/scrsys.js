/* BMPROJECT FILE: videoui/scrsys.js - Various functions used in screen maps

CONTENT:
	This script contains various functions that are called from screen
	maps (maps in the screen folder). Those maps are not actual maps.
	They contain code and are executed on map entry. No entities should
	be present on these maps, as they are mostly option screens. This
	is so that we don't have to shutdown the map engine when changing,
	loading, or exiting.

LICENSE(S):
	GNU General Public License (see zeroinit.js)

*/

function main_title() { // used in main.rmp
	//nestmenu1.go();
	//GarbageCollect();
}