const ScreenWidth = GetScreenWidth();
const ScreenHeight = GetScreenHeight();

function GetScreenBuffer() {
	return GrabImage(0,0,ScreenWidth,ScreenHeight);
}

const BlankBuffer = CreateSurface(GetScreenWidth(),GetScreenHeight(),CreateColor(0,0,0)).createImage();

function ClearScreenBuffer() {
	void BlankBuffer.blit(0,0);
}

function FlushScreenBuffer() {
	ClearScreenBuffer();
	FlipScreen();
}

function ResetClippingRectangle() {
	SetClippingRectangle(0,0,ScreenWidth,ScreenHeight);
}

function BlitBackImage(image) {
	var ss = GetScreenBuffer();
	ClearScreenBuffer();
	image.blit(0,0);
	var meta = GrabImage(0,0,image.width,image.height);
	ClearScreenBuffer();
	ss.blit(0,0);
	return meta;
}

// These probably need moved to like... mapsys.js or something

function GetLayerIndex(name) {
	var magical = GetNumLayers();
	for(var i = 0; i < magical; i++) {
		if(name == GetLayerName(i)) return i;
	}
	return false;
}

function GetPersonScreenX(name) {
	return MapToScreenX(GetLayerIndex("Base"),GetPersonX(name));
}

function GetPersonScreenY(name) {
	return MapToScreenY(GetLayerIndex("Base"),GetPersonY(name));
}

try {notafunction();} catch(k) {TotalLineCount += k.lineNumber+1; FileLineCount[k.fileName.replace(/scripts\//i,"")] = k.lineNumber+1;}