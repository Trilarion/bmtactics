/* BMPROJECT FILE: zeroinit.js - Main Script and game()

SYNOPSIS:
	This file is the main script in BM. It contains the game() function.

EXECUTION:
	The entire program spawns from this script. Everything is handled on
	the lowest level here. game() controls how the program starts, and then
	continues in-game execution from there.

CONTENT:
	Contains game() and some sparse variables. Also evaluates exectrl.js,
	which loads all of the included scripts and  general global variables.

NOTES:
	All headers should be tested here first and upon completion moved to
	exectrl.js.

LICENSE(S):
	GNU General Public License

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,  or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful, 
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not,  write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

// Betrayer's Moon -- main.js - dremelofdeath2K5GPL
// (This project was started on Tuesday,  April 5,  2005.)
// That is, the official Sphere version of this project...

/* The idea behind version 0.2.0
"A designer knows he has achieved perfection not when there is nothing
left to add, but when there is nothing left to take away."
                      --Antoine de Saint-Exupery

(My mother would be proud of me.)
*/

/*LoadWindowStyle("Chara1.rws").drawWindow(2,2,500,50);
LoadFont("FF6font.rfn").drawZoomedText(10,10,1.5,"I don't think that this is a good idea.");
LoadFont("FF6font.rfn").drawText(10,35,"Oh yeah, baby... new features!");
FlipScreen();
GetKey();*/

var TotalLineCount = 0;
var FileLineCount = new Array();

// All of the zeroinit.js headers are now located in exectrl.js.
EvaluateScript("base/exectrl.js");

/*////////////////////////////////////////////////

I want to put this here for now.

const redstar = LoadImage("stars/redstar.png");
const orangestar = LoadImage("stars/ongstar.png");
const yellowstar = LoadImage("stars/ylwstar.png");
const greenstar = LoadImage("stars/grnstar.png");
const bluestar = LoadImage("stars/blustar.png");
const purplestar = LoadImage("stars/pplstar.png");

////////////////////////////////////////////////*/

// ForceSoftRestart will start the game() almost from the top again... it won't retouch the starter utility
// It is left out of exectrl.js because it directly pertains to game()
var ForceSoftRestart;

var escmenu = new Menu();
var debug = new Menu();
escmenu.setTitle("Escape Menu");
debug.setTitle("Debug Menu");
escmenu.addMenuAsItem("Debug Menu",debug);
debug.addItem("Create Ripley",function() {CreatePerson("ripley.rss","ripley.rss",false); AttachInput("ripley.rss");});
debug.addItem("Blood Box",bloodbox);
debug.addItem("Night Box",nightbox);
debug.addItem("Night Box Loop",nightboxloop);
debug.addItem("Crash Game","ASDFjkl");

function bloodbox() {
	var alpha = 255;
	//Rectangle(200,100,200,150,CreateColor(255,0,0));
	GradientRectangle(200,100,200,150,
		CreateColor(100,0,0,alpha),
		CreateColor(100,0,0,alpha),
		CreateColor(200,0,0,alpha),
		CreateColor(200,0,0,alpha));
	GradientRectangle(200+1,100+1,200-2,150-2,
		CreateColor(200,0,0,alpha),
		CreateColor(200,0,0,alpha),
		CreateColor(100,0,0,alpha),
		CreateColor(100,0,0,alpha));
	FlipScreen();
	ClearKeyQueue();
	GetKey();
}

function nightbox() {
	var alpha = 255;
	Rectangle(200,100,200,150,CreateColor(60,60,60));
	GradientRectangle(200+1,100+1,200-2,150-2,
		CreateColor(0,0,0,alpha),
		CreateColor(31,31,31,alpha),
		CreateColor(32,63,94,alpha),
		CreateColor(31,31,31,alpha));
	FlipScreen();
	ClearKeyQueue();
	GetKey();
}

function nightboxloop() {
	var alpha = 0;
	ClearKeyQueue();
	var key = 11111;
	while(key != KEY_Z) {
		while(alpha < 255 || key != KEY_Z) {
			RenderMap();
			if(AreKeysLeft()) key = GetKey();
			alpha+=8;
			Rectangle(200,100,200,150,CreateColor(60,60,60,alpha));
			GradientRectangle(200+1,100+1,200-2,150-2,
				CreateColor(0,0,0,alpha),
				CreateColor(31,31,31,alpha),
				CreateColor(32,63,94,alpha),
				CreateColor(31,31,31,alpha));
			FlipScreen();
		}
		while(alpha > 0 || key != KEY_Z) {
			RenderMap();
			if(AreKeysLeft()) key = GetKey();
			alpha-=8;
			Rectangle(200,100,200,150,CreateColor(60,60,60,alpha));
			GradientRectangle(200+1,100+1,200-2,150-2,
				CreateColor(0,0,0,alpha),
				CreateColor(31,31,31,alpha),
				CreateColor(32,63,94,alpha),
				CreateColor(31,31,31,alpha));
			FlipScreen();
		}
	}
}

function __crazytest() {
	ClearScreenBuffer();
	var sksk = new Menu();
	sksk.setTitle("CONFIRM");
	sksk.addItem("Are you sure? Loading this");
	sksk.addItem("file will leave the current");
	sksk.addItem("game.");
	sksk.addItem("");
	sksk.addItem("Okay!");
	sksk.addItem("Never mind.");
	var deltay = (ScreenHeight-40)/4;
	(new Box([200,0,0],[200,0,0],[100,0,0],[100,0,0])).drawWindow(0,0,ScreenWidth,40);
	TextFont.drawText(5,15,"Please select a file to load.");
	(new Box([0,0,0],[31,31,31],[32,63,94],[31,31,31])).drawWindow(0,40,ScreenWidth,deltay);
	TextFont.drawText(5,46,"Damascus/Front Gate");
	TextFont.drawText(5+ScreenWidth/3,46,"Play Time: 05:26:33");
	TextFont.drawText(5,57,"Ripley");
	TextFont.drawText(5+80,57,"Level 21");
	TextFont.drawText(5+160,57,"120/120");
	/*(new Box([120,99,10],[29,22,5],[117,92,7],[50,44,0])).drawWindow(0,40+deltay,ScreenWidth,deltay);
	(new Box([75,12,75],[0,0,0],[0,0,0],[100,40,120])).drawWindow(0,40+deltay*2,ScreenWidth,deltay);
	(new Box([200,50,5],[0,0,0],[0,0,0],[0,0,0])).drawWindow(0,40+deltay*3,ScreenWidth,deltay);*/
	(new Box([75,12,75],[0,0,0],[0,0,0],[100,40,120])).drawWindow(0,40+deltay,ScreenWidth,deltay);
	TextFont.drawText(5,68,"Taylor");
	TextFont.drawText(5+80,68,"Level 19");
	TextFont.drawText(5+160,68,"72/72");
	(new Box([75,12,75],[0,0,0],[0,0,0],[100,40,120])).drawWindow(0,40+deltay*2,ScreenWidth,deltay);
	TextFont.drawText(5,79,"Sampson");
	TextFont.drawText(5+80,79,"Level 19");
	TextFont.drawText(5+160,79,"118/118");
	(new Box([75,12,75],[0,0,0],[0,0,0],[100,40,120])).drawWindow(0,40+deltay*3,ScreenWidth,deltay);
	TextFont.drawText(5,90,"Dale");
	TextFont.drawText(5+80,90,"Level 20");
	TextFont.drawText(5+160,90,"103/103");
	var ss = GetScreenBuffer();
	ClearScreenBuffer();
	ss.blitMask(0,0,CreateColor(100,100,100));
	sksk.go(ScreenWidth*3/5,40+deltay/2);
}

debug.addItem("Crazy Test",__crazytest);
					
					setpostnotecustomdisp("Current postnotes for "+GameTitle+":");
					setpostnotefont(TextFont);
					/*try {_notafunction();} catch(error) {postnote("Hey, fix that.",error);}
					try {_notafunction();} catch(error) {postnote("And that.",error);}
					try {_notafunction();} catch(error) {postnote("And that too.",error);}
					try {_notafunction();} catch(error) {postnote("And, while you're at it, think about fixing that.",error);}
*/
// game() section. First function to execute...
function game() {
	show_postnotes();
	try { // start main error protection
		check_die_flag(); // ugly ack to fix Exit() crash before code execution (spheredev.net forums ID #1056)
		do {
			GarbageCollect(); // free startup memory
			ForceSoftRestart = false;
			var decisionid = 0;
			if(ShowDeveloperMenu) decisionid = DeveloperMenu();
			switch(decisionid) {
				case 0: // load the game normally
					// This is where the the game will start normally (i.e. without the devmenu)
					MapEngine(/*"screen/main.rmp"*/"testc.rmp",60);
					break;
				case 1: // load the game, but in debug mode
					break;
				case -1: // only load the test map
					break;
			}
		} while(ForceSoftRestart);
	} catch(error) {
		HandleException(error);
		Exit();
	} // End error handler protection
} // game()
// This last line is for the line counter routine... leave it alone... you should see it at the bottom of every file
try {notafunction();} catch(k) {TotalLineCount += k.lineNumber+1; FileLineCount[k.fileName.replace(/scripts\//i,"")] = k.lineNumber+1;}